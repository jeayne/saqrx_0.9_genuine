# butt2.plt 31.03.2014 10:24

Fs            = 48000.0 # Hz
real_freq_3db = 25.0    # Hz

fnormed = (2*real_freq_3db)/Fs
	
#--- prototyp analog filter is Butterworth 2.order, Omega=1 normed :	
	
an0= 1.0
an1= 0.0
an2= 0.0

bn0 = 1.0
bn1 = sqrt(2.0)
bn2 = 1.0

#-----------------------------------------
#
#
#/* f1_normed = f1/(Fs/2) normed :  */
#
#void Biquad_construct (	double f1_normed, biquad_struct * Biq )
#{ 	double	c,s,rr,cc,ss,sc,pp ;
#
#
#/*                  an0 + an1*s + an2*s^2
#	Analog: H(s)=	----------------------
#                    bn0 + bn1*s + bn2*s^2
#
#
#                    A0 + A1*z^-1 + A2*z^-2
#	Digital:H(z)=	-----------------------
#                    B0 + B1*z^-1 + B2*z^-2	   ; B0 = 1 normed !
#
#*/
#
#
#/* gamma of Bilinear-Transf.*/

rr = (pi/2)* fnormed
c  = cos(rr)
s  = sin(rr)		

#	/* Bilinear-Transformation ausf�hren:				*/

cc = c*c
ss = s*s
sc = s*c

A2 = an2*cc + an0*ss
pp = an1*sc
A0 = pp + A2
A2 = A2 - pp
A1 = 2*( an0*ss -an2*cc)

B2 = bn2*cc + bn0*ss
pp = bn1*sc
B0 = pp + B2
B2 = B2 - pp
B1 = 2*( bn0*ss -bn2*cc)

A2 = A2/B0
B2 = B2/B0 
A0 = A0/B0
A1 = A1/B0
B1 = B1/B0

print(sprintf("B0 = %23.18e\n", B0 ))


#sprintf("\n")
#print(sprintf("A0,A1,A2 = %23.18f,%23.18f,%23.18f\n", A0,A1,A2 ))

print(sprintf("A0,A1,A2 = %23.18e,%23.18e,%23.18e\n", A0,A1,A2 ))
print(sprintf("B0,B1,B2 = %23.18e,%23.18e,%23.18e\n",1.0,B1,B2 ))

				
#}	/* Biquad_construct */

j = {0,1}   # imaginary unit


### normal DC Lowpass filter:

H(z) = (A0*z*z + A1*z + A2)/( z*z + B1*z + B2)	

Hdb( f ) = 20.0*log10( abs(H( exp((2*pi*f)/Fs*j) )))
#########################


########  complex filter:

Fshift = 750.0
lambda = exp((2*pi*Fshift)/Fs*j)
CA2 = A2*lambda*lambda
CB2 = B2*lambda*lambda 
CA0 = A0
CA1 = A1*lambda
CB1 = B1*lambda

CH(z) = (CA0*z*z + CA1*z + CA2)/( z*z + CB1*z + CB2)	
CHdb( f ) = 20.0*log10( abs(CH( exp((2*pi*f)/Fs*j) )))
#################################



### normal lowpass :

print(Hdb( 0 ))	
print(Hdb( real_freq_3db))
print(Hdb( 2* real_freq_3db))	
print(Hdb( 4* real_freq_3db))
print(Hdb( 8* real_freq_3db))	

### complex filter:
print("\n Complex filter: ")
print(CHdb( 0.0 ))
print(CHdb( Fshift ))	
print(CHdb( -Fshift ))	
print(CHdb( Fshift+real_freq_3db))
print(CHdb( Fshift-real_freq_3db))

print(CHdb( Fshift+2* real_freq_3db))
print(CHdb( -Fshift+2* real_freq_3db))
	

print(CHdb( Fshift+4* real_freq_3db))
print(CHdb( -Fshift+4* real_freq_3db))

print(CHdb( Fshift+8* real_freq_3db))	
print(CHdb( -Fshift+8* real_freq_3db))	

###

print(sprintf("CA0_RE, CA0_IM = %23.18e,%23.18e\n", real(CA0), imag(CA0) ))
print(sprintf("CA1_RE, CA1_IM = %23.18e,%23.18e\n", real(CA1), imag(CA1) ))
print(sprintf("CA2_RE, CA2_IM = %23.18e,%23.18e\n", real(CA2), imag(CA2) ))

print(sprintf("CB1_RE, CB1_IM = %23.18e,%23.18e\n", real(CB1), imag(CB1) ))
print(sprintf("CB2_RE, CB2_IM = %23.18e,%23.18e\n", real(CB2), imag(CB2) ))

############ image:
set dummy x
set grid
# set offsets
set nolog
set nopolar
set title "IQ filter Butterworth/Legendre BW= 50 Hz (-3dB) "
set xlabel "Frequency [Hz]"

#set logscale x 2
#set xrange [13.75 : 22050.0]
#set xtics 13.75, 2

set xrange [600 : 900]
set xtics 600, 10


#set ylabel "dB"
#set yrange [-100.0 : 0.0 ]
#set ytics -100.0, 10.0

# "stagger2.mag" using 1:2 with lines,
# CHdb(x)  with lines,
# "stagger2.mag" using 1:2 with lines,


set ylabel "dB" textcolor rgb "red"
set yrange [-120.0 : 3.0 ]
set ytics -120.0, 5.0  textcolor rgb "red"

set y2label "Passband dB" textcolor rgb "blue"
set y2range [-3.0 : 0.075 ]
set y2tics -3.0, 0.5 textcolor rgb "blue"
#set y2tics -3.0, 0.5

#set key default
#set key outside bottom center box
#set key outside rmargin tmargin center box

set key below

plot "cbutter2.mag" using 1:2 with lines lt 1, "" using 1:2 axes x1y2 notitle  with lines lt 1,\
     "cbutter4.mag" using 1:2 with lines lt 2, "" using 1:2 axes x1y2 notitle  with lines lt 2,\
     "clfilter4.mag" using 1:2 with lines lt 3, "" using 1:2 axes x1y2 notitle  with lines lt 3

#show output
pause -1 "Hit return to continue"

set xrange [700 : 800]
set xtics 700, 5


set ylabel "dB" #textcolor rgb "red"
set yrange [-6.0 : 0.5 ]
set ytics -6.0, 0.5  #textcolor rgb "red"

plot "cbutter4.mag"  using 1:2 with lines lt 1,\
     "cbutter6.mag"  using 1:2 with lines lt 3,\
     "cbutter8.mag"  using 1:2 with lines lt 5,\
     "cbutter10.mag"  using 1:2 with lines lt 6,\
     "cbutter12.mag"  using 1:2 with lines lt 7               


#     "clfilter4.mag" using 1:2 with lines lt 2,\
#     "clfilter6.mag" using 1:2 with lines lt 4,\
#     "clfilter8.mag" using 1:2 with lines lt 8

pause -1 "Hit return to continue"
