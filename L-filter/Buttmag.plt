# Buttmag.plt   27.11.2014 18:17

reset
set grid

          
set linetype 1 lc rgb "dark-violet" lw 2 pt 0
set linetype 2 lc rgb "sea-green"   lw 2 pt 7
set linetype 3 lc rgb "cyan"        lw 2 pt 6 pi -1
set linetype 4 lc rgb "dark-red"    lw 2 pt 5 pi -1
set linetype 5 lc rgb "blue"        lw 2 pt 8
set linetype 6 lc rgb "dark-orange" lw 2 pt 3
set linetype 7 lc rgb "black"       lw 2 pt 11
set linetype 8 lc rgb "goldenrod"   lw 2
set linetype cycle 8








set title "Butterworth/Legendre Bandpass , BW= 20 Hz (+- 10Hz for -3dB) "
set xlabel "Frequency [Hz]"

#set logscale x 2
#set xrange [13.75 : 22050.0]
#set xtics 13.75, 2

set xrange [-50 : 50]
set xtics -50, 10
set mxtics 10

#set ylabel "dB"
#set yrange [-100.0 : 0.0 ]
#set ytics -100.0, 10.0



set ylabel "dB"  # textcolor rgb "red"
set yrange [-120.0 : 3.0 ]
set ytics -120.0, 10.0  # textcolor rgb "red"
set y2range [-120.0 : 3.0 ]
set y2tics -120.0, 10.0  # textcolor rgb "red"

#set y2label "Passband dB" textcolor rgb "blue"
#set y2range [-3.0 : 0.075 ]
#set y2tics -3.0, 0.5 textcolor rgb "blue"
#set y2tics -3.0, 0.5

#set key default
#set key outside bottom center box
#set key outside rmargin tmargin center box

set key below

set title "Butterworth/Legendre/Tschebyscheff/Inv.T(-90db) (0.25dB, 0.03468dB ripple) Bandpass , BW= 20 Hz (+- 10Hz for -3dB) "
plot "butt10.mag"     using 1:2 t "BUTT 10-POL" with lines,\
     "legendre10.mag" using 1:2 t "LEG 10-POL" with lines,\
     "tsch10e.mag"    using 1:2 t "TSCH 10-POL equ" with lines,\
     "itsch10.mag"    using 1:2 t "INVT 10-POL" with lines,\
     "itsch9.mag"     using 1:2 t "INVT 9-POL" with lines,\
     "tsch10.mag"     using 1:2 t "TSCH 10-POL" with lines
     
pause -1 "Hit return to continue"

plot "butt2.mag"     using 1:2 t "BUTT 2-POL" with lines lt 1,\
     "butt4.mag"     using 1:2 t "BUTT 4-POL" with lines lt 2,\
     "butt6.mag"     using 1:2 t "BUTT 6-POL" with lines lt 6,\
     "butt8.mag"     using 1:2 t "BUTT 8-POL" with lines lt 7,\
     "butt10.mag"    using 1:2 t "BUTT 10-POL" with lines lt 8,\
     "butt12.mag"    using 1:2 t "BUTT 12-POL" with lines lt 9
                            
pause -1 "Hit return to continue"

plot "legendre4.mag"  using 1:2 t "LEG 4-POL" with lines lt 3,\
     "legendre6.mag"  using 1:2 t "LEG 6-POL" with lines lt 4,\
     "legendre8.mag"  using 1:2 t "LEG 8-POL" with lines lt 5,\
     "legendre10.mag" using 1:2 t "LEG 10-POL" with lines lt 6
                                
pause -1 "Hit return to continue"

plot "butt8.mag"      using 1:2 t "BUTT 8-POL" with lines lt 7,\
     "legendre8.mag"  using 1:2 t "LEG  8-POL" with lines lt 6
pause -1 "Hit return to continue"

plot "butt8.mag"      using 1:2 t "BUTT 8-POL" with lines lt 7,\
     "legendre6.mag"  using 1:2 t "LEG 6-POL" with lines lt 6
pause -1 "Hit return to continue"

set xrange [-12 : 12]
set xtics -12, 1


set ylabel "dB" #textcolor rgb "red"
set yrange [-6.0 : 0.5 ]
set ytics -6.0, 0.5  #textcolor rgb "red"
set y2range [-6.0 : 0.5 ]
set y2tics -6.0, 0.5  #textcolor rgb "red"

plot "butt4.mag"     using 1:2 t "BUTT 4-POL" with lines lt 1,\
     "butt6.mag"     using 1:2 t "BUTT 6-POL" with lines lt 3,\
     "butt8.mag"     using 1:2 t "BUTT 8-POL" with lines lt 5,\
     "butt10.mag"    using 1:2 t "BUTT 10-POL" with lines lt 6,\
     "butt12.mag"    using 1:2 t "BUTT 12-POL" with lines lt 7,\
     "legendre4.mag" using 1:2 t "LEG 4-POL" with lines lt 2,\
     "legendre6.mag" using 1:2 t "LEG 6-POL" with lines lt 4,\
     "legendre8.mag" using 1:2 t "LEG 8-POL" with lines lt 8

pause -1 "Hit return to continue"

plot "butt4.mag"     using 1:2 t "BUTT 4-POL" with lines,\
     "legendre4.mag" using 1:2 t "LEG 4-POL" with lines

pause -1 "Hit return to continue"

plot "butt6.mag"     using 1:2 t "BUTT 6-POL" with lines,\
     "legendre6.mag" using 1:2 t "LEG 6-POL" with lines
          
pause -1 "Hit return to continue"


plot "butt8.mag"     using 1:2 t "BUTT 8-POL" with lines,\
     "legendre8.mag" using 1:2 t "LEG 8-POL" with lines
     
pause -1 "Hit return to continue"

set title "Butterworth/Legendre/Tschebyscheff/InvT (0.25dB, 0.03468dB ripple) Bandpass , BW= 20 Hz (+- 10Hz for -3dB) "
plot "butt10.mag"     using 1:2 t "BUTT 10-POL" with lines,\
     "legendre10.mag" using 1:2 t "LEG 10-POL" with lines,\
     "tsch10e.mag"    using 1:2 t "TSCH 10-POL equ" with lines,\
     "itsch10.mag"    using 1:2 t "ITSCH 10-POL" with lines,\
     "tsch10.mag"     using 1:2 t "TSCH 10-POL" with lines
     
pause -1 "Hit return to continue"
