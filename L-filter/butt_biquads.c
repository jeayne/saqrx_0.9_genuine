/*	butt_biquads.c      28.03.2014 10:06		

    Roland Fr�hlich	

*/

#define Fft_Fs          48000.0
#define real_freq_3db   10.0
#define Fshift          750.0

#include <math.h>
#include <complex.h>

#include <stdio.h>

#define eprint printf

#undef	STEREO_FILTER_IMPLEMENTED

typedef complex stereo_sample ;

typedef struct biquad_struc
{	int		section;				// last filter has 0 here
	double	A0,A1,A2,B0,B1,B2,an2,an1,an0,bn2,bn1,bn0 ;	// Coeffs
	double	Lx_2,Lx_1,Ly_2,Ly_1;	// data of difference equation, Left channel
#ifdef STEREO_FILTER_IMPLEMENTED	
	double	Rx_2,Rx_1,Ry_2,Ry_1;	// right channel
#endif	
} biquad_struct;


#define BUTTERMAX 4	// 4 biquads for maximal order=8.

biquad_struct	Butterworth_LP [ BUTTERMAX ]; 
biquad_struct	Butterworth_HP [ BUTTERMAX ];


/* f1_normed = f1/(Fs/2) normed :  */

void Biquad_construct (	double f1_normed, biquad_struct * Biq )
{ 	double	c,s,rr,cc,ss,sc ;


/*                  an0 + an1*s + an2*s^2
	Analog: H(s)=	----------------------
                    bn0 + bn1*s + bn2*s^2


                    A0 + A1*z^-1 + A2*z^-2
	Digital:H(z)=	-----------------------
                    B0 + B1*z^-1 + B2*z^-2	   ; B0 = 1 normed !

*/


/* gamma of Bilinear-TrBiq->ansf.*/

	rr = (M_PI/2)*f1_normed;
	c  = cos(rr); s = sin(rr);		

	/* Bilinear-Transformation ausf�hren:				*/

	cc = c*c; ss = s*s; sc = s*c;

	Biq->A0 = (rr=Biq->an1*sc) + ( Biq->A2= Biq->an2*cc + Biq->an0*ss);
	Biq->A2 = Biq->A2 - rr;
	Biq->A1 = 2*( Biq->an0*ss -Biq->an2*cc);

	Biq->B0 = (rr=Biq->bn1*sc) + ( Biq->B2= Biq->bn2*cc + Biq->bn0*ss);
	Biq->B2 = Biq->B2 - rr;
	Biq->B1 = 2*( Biq->bn0*ss -Biq->bn2*cc);

	Biq->A2 = Biq->A2/Biq->B0; Biq->B2 = Biq->B2/Biq->B0; 
	Biq->A0 = Biq->A0/Biq->B0; Biq->A1 = Biq->A1/Biq->B0;  Biq->B1 = Biq->B1/Biq->B0; 

	Biq->Lx_2 = Biq->Lx_1 = Biq->Ly_2 = Biq->Ly_1 = 0.0;	// data init Left
#ifdef STEREO_FILTER_IMPLEMENTED
	Biq->Rx_2 = Biq->Rx_1 = Biq->Ry_2 = Biq->Ry_1 = 0.0;	// data init Right
#endif

	eprint("\n");
	eprint("A0,A1,A2 = %13.9lf,%13.9lf,%13.9lf\n",Biq->A0,Biq->A1,Biq->A2 );
	eprint("B0,B1,B2 = %13.9lf,%13.9lf,%13.9lf\n",    1.0,Biq->B1,Biq->B2 );

				
}	/* Biquad_construct */

void Clear_Butterworth_Filter ( biquad_struct * B)
{	int test;	 	
	do
	{	
		B->Lx_2 = B->Lx_1 = B->Ly_2 = B->Ly_1 = 0.0;	// data init Left
#ifdef STEREO_FILTER_IMPLEMENTED		
		B->Rx_2 = B->Rx_1 = B->Ry_2 = B->Ry_1 = 0.0;	// data init Right
#endif		
		if ((test=B->section) > 0) B++;	
	} while (test > 0);	
	
}	// Clear_Butterworth_Filter ()



//	B = & Butterworth_LP [0]
// 	absolute -3dB corner frequency in Hz
//	order (even!) = 2,4,6,8.
//	hipass=0 for LP, hipass !=0 for HP

void Build_Butterworth_Filter ( biquad_struct * B, double real_freq_3db, int order, int hipass )	
{	int i,anz_quads; 
	double fnormed,r;
		
	anz_quads = order/2;
	if (anz_quads > BUTTERMAX) anz_quads = BUTTERMAX;
	if (anz_quads < 1) anz_quads = 1;

	fnormed = (2*real_freq_3db)/Fft_Fs;
	r = M_PI/(4*anz_quads);	

	for (i=0; i < anz_quads; i++)
	{	// 2. order Polynomfilter	
		B->section = anz_quads-1 -i;
		if (hipass )
		{ B->an0 = B->an1 = 0.0; B->an2= 1.0; }	
		else
		{ B->an2 = B->an1 = 0.0; B->an0= 1.0; }	
		B->bn2 = B->bn0 = 1.0;
		B->bn1 = 2 * sin( (2*i+1)*r );
		Biquad_construct (	fnormed, B );	// build coeffs + init data
		B++;
	}
	
}	// Build_Butterworth_Filter()

// Make Filters sample process here:
#ifdef STEREO_FILTER_IMPLEMENTED
stereo_sample Butter_Perform_Stereo ( stereo_sample input, biquad_struct * B )
{	stereo_sample result;
	int test;
	
	do
	{
// left channel:
	result.FRE = B->A0*input.FRE + B->A1*B->Lx_1 + B->A2*B->Lx_2 - B->B1*B->Ly_1 - B->B2*B->Ly_2;
	B->Lx_2 = B->Lx_1;
	B->Lx_1 = input.FRE;
	B->Ly_2 = B->Ly_1;
	B->Ly_1 = input.FRE = result.FRE;	// setup input for next stage
// right channel:
	result.FIM = B->A0*input.FIM + B->A1*B->Rx_1 + B->A2*B->Rx_2 - B->B1*B->Ry_1 - B->B2*B->Ry_2;
	B->Rx_2 = B->Rx_1;
	B->Rx_1 = input.FIM;
	B->Ry_2 = B->Ry_1;
	B->Ry_1 = input.FIM = result.FIM;	// setup input for next stage	
	
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return result;				// last cascaded output to caller
	
}	// Butter_Perform_Stereo()
#endif	// #ifdef STEREO_FILTER_IMPLEMENTED


// Left=LP-out, Right=HP-Out :
stereo_sample Butter_Perform_Splitter ( float mono_input )
{	stereo_sample result;
	int test;
	biquad_struct * B = & Butterworth_LP [0];	
	do
	{
	// left channel:
	result.FRE = B->A0*mono_input + B->A1*B->Lx_1 + B->A2*B->Lx_2 - B->B1*B->Ly_1 - B->B2*B->Ly_2;
	B->Lx_2 = B->Lx_1;
	B->Lx_1 = mono_input;
	B->Ly_2 = B->Ly_1;
	B->Ly_1 = mono_input = result.FRE;	// setup mono_input for next stage	
	if ((test=B->section) > 0) B++;	
	} while (test > 0);

	B = & Butterworth_HP [0];	
	do
	{
	// left channel:
	result.FIM = B->A0*mono_input + B->A1*B->Lx_1 + B->A2*B->Lx_2 - B->B1*B->Ly_1 - B->B2*B->Ly_2;
	B->Lx_2 = B->Lx_1;
	B->Lx_1 = mono_input;
	B->Ly_2 = B->Ly_1;
	B->Ly_1 = mono_input = result.FIM;	// setup mono_input for next stage	
	if ((test=B->section) > 0) B++;	
	} while (test > 0);

	return result;				// last output to caller
	
}	// Butter_Perform_Splitter()

int main(int argc, char *argv[]) 
{



	return 0;
}


	