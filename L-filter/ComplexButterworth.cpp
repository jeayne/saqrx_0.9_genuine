/*	ComplexButterworth.cpp      27.11.2014 08:17		
    Make & perform complex filter
    
    Roland Fr�hlich	

*/

#define Fft_Fs          48000.0
#define freq_3db        10.0		// 20Hz bandwidth
#define Fshift          750.0


#include <stdio.h>
#include <complex.h>
#include <math.h>

#include	<stdarg.h>	// for eprint()

#include "Tschebyscheff_Coeff.h"
#include "ITschebyscheff_Coeff.h"

// equal with L10 at j*2 : ripple = 0.03468024784745598392299711899 dB.
double Tschebyscheff_10equ[] = { 
// T10 Frequency response at j*2 =  -92.13 dB.
// Tschebyscheff filter analoge biquads, ripple=  0.035 dB,order= 10 : index=[0] 
   0.112991175215746983,   0.000000000000000000,   0.000000000000000000, // Nom  = an0 +an1*s +an2*s^2
   0.112991175215746983,   0.595024459746396942,   1.000000000000000000, // Denom= bn0 +bn1*s +bn2*s^2
// Zero (+-Conj)= -0.2975122298731984707621520973 + 0.1564533422194108781693291489*I
// |Hmax|=   0.000 dB,gmax(denom)=  18.939 dB at F=  0.0000 Hz , Q=   0.565

   0.278190128974620037,   0.000000000000000000,   0.000000000000000000, // Nom  = an0 +an1*s +an2*s^2
   0.278190128974620037,   0.536779319847231286,   1.000000000000000000, // Denom= bn0 +bn1*s +bn2*s^2
// Zero (+-Conj)= -0.2683896599236156427878119022 + 0.4540452834472636385490041282*I
// |Hmax|=   1.149 dB,gmax(denom)=  12.262 dB at F=  0.3662 Hz , Q=   0.983

   0.545487651062398838,   0.000000000000000000,   0.000000000000000000, // Nom  = an0 +an1*s +an2*s^2
   0.545487651062398838,   0.425990480159982603,   1.000000000000000000, // Denom= bn0 +bn1*s +bn2*s^2
// Zero (+-Conj)= -0.2129952400799913016347447673 + 0.7071921088117893480786313075*I
// |Hmax|=   5.157 dB,gmax(denom)=  10.421 dB at F=  0.6744 Hz , Q=   1.734

   0.812785173150177640,   0.000000000000000000,   0.000000000000000000, // Nom  = an0 +an1*s +an2*s^2
   0.812785173150177640,   0.273502724224474293,   1.000000000000000000, // Denom= bn0 +bn1*s +bn2*s^2
// Zero (+-Conj)= -0.1367513621122371464050950662 + 0.8911140432686634698468592004*I
// |Hmax|=  10.462 dB,gmax(denom)=  12.262 dB at F=  0.8806 Hz , Q=   3.296

   0.977984126909050694,   0.000000000000000000,   0.000000000000000000, // Nom  = an0 +an1*s +an2*s^2
   0.977984126909050694,   0.094242616036342655,   1.000000000000000000, // Denom= bn0 +bn1*s +bn2*s^2
// Zero (+-Conj)= -0.04712130801817132725530950837 + 0.9878075264137783012071572323*I
// |Hmax|=  20.428 dB,gmax(denom)=  20.622 dB at F=  0.9867 Hz , Q=  10.493

};
// Legendre, Butterworth, Tschebyscheff (0.5dB pp-ripple) filters, order 2 .. 20.
// Frequency Response ( -3dB at BW) at octave = 2*BW :
// L2   =  -12.30 dB.  B2  =   -12.30 dB.   T2   =  -13.67 dB.   
// L3   =  -21.73 dB.  B3  =   -18.13 dB.   T3   =  -22.58 dB. 
// L4   =  -30.31 dB.  B4  =   -24.10 dB.   T4   =  -32.55 dB. 
// L5   =  -40.76 dB.  B5  =   -30.11 dB.   T5   =  -43.04 dB. 
// L6   =  -50.27 dB.  B6  =   -36.12 dB.   T6   =  -53.81 dB. 
// L7   =  -61.01 dB.  B7  =   -42.14 dB.   T7   =  -64.76 dB. 
// L8   =  -70.98 dB.  B8  =   -48.16 dB.   T8   =  -75.82 dB. 
// L9   =  -81.88 dB.  B9  =   -54.19 dB.   T9   =  -86.97 dB. 
// L10  =  -92.13 dB.  B10 =   -60.21 dB.   T10  =  -98.17 dB. 
// L11  = -103.13 dB.  B11 =   -66.23 dB.   T11  = -109.41 dB. 
// L12  = -113.57 dB.  B12 =   -72.25 dB.   T12  = -120.68 dB. 
// L13  = -124.64 dB.  B13 =   -78.27 dB.   T13  = -131.98 dB. 
// L14  = -135.22 dB.  B14 =   -84.29 dB.   T14  = -143.30 dB. 
// L15  = -146.33 dB.  B15 =   -90.31 dB.   T15  = -154.64 dB. 
// L16  = -157.02 dB.  B16 =   -96.33 dB.   T16  = -165.98 dB. 
// L17  = -168.17 dB.  B17 =  -102.35 dB.   T17  = -177.34 dB. 
// L18  = -178.94 dB.  B18 =  -108.37 dB.   T18  = -188.71 dB. 
// L19  = -190.12 dB.  B19 =  -114.39 dB.   T19  = -200.08 dB. 
// L20  = -200.95 dB.  B20 =  -120.41 dB.   T20  = -211.46 dB. 
// this list was computed with PARI/LFilter.gp 14.12.2014 18:35





FILE	*FTRACE;

static int eprint (char *fmt, ...)
{
	va_list argptr;
	int cnt;
	char buffer[5*80];	/* 5 Zeilen Buffer, m��te gen�gen */

	va_start(argptr, fmt);
	cnt = vsprintf(buffer, fmt, argptr);
	va_end(argptr);

	printf("%s", buffer);		/* immer auf Bildschirm */
	fprintf(FTRACE,"%s", buffer);	/* und hier auf File */
	return(cnt);	
}



typedef struct Cbiquad_struc_tag
{	int		section;				            // last filter has 0 here
	double	A0,A1,A2,B0,B1,B2,an2,an1,an0,bn2,bn1,bn0 ;	// Coeffs
	
	double _Complex CA0,CA1,CA2,CB1,CB2;        // complex coeffs of difference equation	
	double _Complex CLx_2,CLx_1,CLy_2,CLy_1;	// complex data   of difference equation	

} Cbiquad_struct;


#define BIQUADMAX 6	// 6 biquads for maximal order=12.

Cbiquad_struct	Filter1 [ BIQUADMAX ]; 
Cbiquad_struct	Filter2 [ BIQUADMAX ]; 

/* f1_normed = f1/(Fs/2); normed :  */
/* shift_normed = ((2*M_PI*Fshift)/Fft_Fs); */

void Biquad_construct (	double f1_normed, double shift_normed, Cbiquad_struct * Biq )
{ 	double	c,s,rr,cc,ss,sc ;
    double _Complex lambda = cexp( shift_normed * I);  // e^it

/*                  an0 + an1*s + an2*s^2
	Analog: H(s)=	----------------------
                    bn0 + bn1*s + bn2*s^2


                    A0 + A1*z^-1 + A2*z^-2
	Digital:H(z)=	-----------------------
                    B0 + B1*z^-1 + B2*z^-2	   ; B0 = 1 normed !

*/

/* gamma of Bilinear-Transf.*/

	rr = (M_PI/2)*f1_normed;
	c  = cos(rr); s = sin(rr);		

	/* Bilinear-Transformation ausf�hren:				*/

	cc = c*c; ss = s*s; sc = s*c;

	Biq->A0 = (rr=Biq->an1*sc) + ( Biq->A2= Biq->an2*cc + Biq->an0*ss);
	Biq->A2 = Biq->A2 - rr;
	Biq->A1 = 2*( Biq->an0*ss -Biq->an2*cc);

	Biq->B0 = (rr=Biq->bn1*sc) + ( Biq->B2= Biq->bn2*cc + Biq->bn0*ss);
	Biq->B2 = Biq->B2 - rr;
	Biq->B1 = 2*( Biq->bn0*ss -Biq->bn2*cc);

	Biq->A2 = Biq->A2/Biq->B0; Biq->B2 = Biq->B2/Biq->B0; 
	Biq->A0 = Biq->A0/Biq->B0; Biq->A1 = Biq->A1/Biq->B0;  Biq->B1 = Biq->B1/Biq->B0; 

	Biq->CLx_2 = Biq->CLx_1 = Biq->CLy_2 = Biq->CLy_1 = 0.0*I;	// complex data init Left	

	eprint("\n");
	eprint("A0,A1,A2 = %23.18le,%23.18le,%23.18le\n",Biq->A0,Biq->A1,Biq->A2 );
	eprint("B0,B1,B2 = %23.18le,%23.18le,%23.18le\n",    1.0,Biq->B1,Biq->B2 );


    Biq->CA2 = Biq->A2*lambda*lambda;
    Biq->CB2 = Biq->B2*lambda*lambda; 
    Biq->CA0 = Biq->A0;
    Biq->CA1 = Biq->A1*lambda;
    Biq->CB1 = Biq->B1*lambda;
    
    eprint("CA0_re = %23.18le , CA0_im = %23.18le\n", __real__ Biq->CA0, __imag__ Biq->CA0);
    eprint("CA1_re = %23.18le , CA1_im = %23.18le\n", __real__ Biq->CA1, __imag__ Biq->CA1);    
    eprint("CA2_re = %23.18le , CA2_im = %23.18le\n", __real__ Biq->CA2, __imag__ Biq->CA2);
    eprint("CB1_re = %23.18le , CB1_im = %23.18le\n", __real__ Biq->CB1, __imag__ Biq->CB1);    
    eprint("CB2_re = %23.18le , CB2_im = %23.18le\n", __real__ Biq->CB2, __imag__ Biq->CB2);
				
}	/* Biquad_construct */

void Clear_Butterworth_Filter ( Cbiquad_struct * B)
{	int test;	 	
	do
	{	
		B->CLx_2 = B->CLx_1 = B->CLy_2 = B->CLy_1 = 0.0*I;	// complex data init 
	
		if ((test=B->section) > 0) B++;	
	} while (test > 0);	
	
}	// Clear_Butterworth_Filter ()



//	B = & Filter1 [0]
// 	absolute -3dB corner frequency in Hz
//	order (even!) = 2,4,6,8.
//	hipass=0 for LP, hipass !=0 for HP

int isum=0;

void Build_Butterworth_Filter ( Cbiquad_struct * B, double iirFs, double iirfreq_3db, double iirshift, int order, int hipass )	
{	int i,anz_quads; 
	double r;
	double fnormed      = (2*iirfreq_3db)/iirFs;
	double shift_normed = ((2*M_PI*iirshift)/iirFs);
			
	anz_quads = order/2;
	if (anz_quads > BIQUADMAX) anz_quads = BIQUADMAX;
	if (anz_quads < 1) anz_quads = 1;

	r = M_PI/(4*anz_quads);	

#if 1		        
    eprint("// Butterworth Order= %d, coeffs at &Butterworth_SCoeffs[%d]\n", order,isum);
#endif

	for (i=0; i < anz_quads; i++)
	{	// 2. order Polynomfilter	
		B->section = anz_quads-1 -i;
		if (hipass )
		{ B->an0 = B->an1 = 0.0; B->an2= 1.0; }	
		else
		{ B->an2 = B->an1 = 0.0; B->an0= 1.0; }	
		B->bn2 = B->bn0 = 1.0;
//		B->bn1 = 2 * sin( (2*i+1)*r );  // highest Q first
		B->bn1 = 2 * cos( (2*i+1)*r );	// lowest  Q first, cf. Tietze-Schenk	
#if 1		        
        double g1 = ((B->bn0 - B->bn2)*(B->bn0 - B->bn2) + B->bn1*B->bn1)/(B->an0*B->an0);    // invers betrag quadrat at 1*J
        g1 = 1/g1;
        double q  = sqrt(B->bn2)/B->bn1;
//    eprint("// Butterworth Order= %d\n", order);
//	eprint("// [%d]\n",isum); isum+=6;
	eprint("%23.18le,%23.18le,%23.18le, // an0,an1,an2 = \n", B->an0,B->an1,B->an2 );
	eprint("%23.18le,%23.18le,%23.18le, // bn0,bn1,bn2 = \n", B->bn0,B->bn1,B->bn2 );
    eprint("// g1 = %13.9lf dB, Q= %13.9lf\n\n", 10*log10(g1), q);
	 isum+=6;	
#endif		
//		Biquad_construct (	fnormed, shift_normed, B );	// build coeffs + init data
		B++;
	}
	
}	// Build_Butterworth_Filter()

//--------- general filter from table:

void Build_Digital_Filter ( Cbiquad_struct * B, double iirFs, double iirfreq_3db, double iirshift, int order, double * anacoeffs )	
{	int i,anz_quads; 

	double fnormed      = (2*iirfreq_3db)/iirFs;
	double shift_normed = ((2*M_PI*iirshift)/iirFs);
			
	anz_quads = order/2;
	if (anz_quads > BIQUADMAX) anz_quads = BIQUADMAX;
	if (anz_quads < 1) anz_quads = 1;		

	for (i=0; i < anz_quads; i++)   // 2. order Polynomfilter
	{		
		B->section = anz_quads-1 -i;
        // read 6 analog coeffs from table:
		B->an0 = *anacoeffs++;
		B->an1 = *anacoeffs++; 
		B->an2 = *anacoeffs++;
		B->bn0 = *anacoeffs++;
		B->bn1 = *anacoeffs++; 
		B->bn2 = *anacoeffs++;		
	
		Biquad_construct (	fnormed, shift_normed, B );	// build coeffs + init data
		B++;
	}
	
}	// Build_Digital_Filter()

double _Complex CFilter_Perform ( double _Complex input, Cbiquad_struct * B )
{	double _Complex result;
	int test;
	
	do
	{   // complex biquad filter operation :
	result = B->CA0* input + B->CA1*B->CLx_1 + B->CA2*B->CLx_2 - B->CB1*B->CLy_1 - B->CB2*B->CLy_2;
	B->CLx_2 = B->CLx_1;
	B->CLx_1 = input;
	B->CLy_2 = B->CLy_1;
	B->CLy_1 = input = result;	// setup input for next stage
	
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return result;				// last cascaded output to caller
	
}	// CFilter_Perform()

// Spectrum plot evaluation:

double  C_Eval_dB ( double freq, double iirFs, Cbiquad_struct * B )
{	
	int test;
    double _Complex CH,z;
    
    z = cexp(((2*M_PI*freq)/iirFs)*I);
    CH = 1.0 + 0.0*I;
    	
	do
	{   // complex biquad filter in serial topology , all multiply :
        CH = CH * (B->CA0*z*z + B->CA1*z + B->CA2)/( z*z + B->CB1*z + B->CB2);		    	
    	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

    return 20.0*log10( cabs(CH));
    	
}	// C_Eval_dB()


//

//---------------------------------------------------

#include "Butterworth_Coeff.h"


//---------------------------------------------------

double LFilter_SCoeffs[] =
{       // N=4
// [0]
4.307915793843129500e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
4.307915793843129500e-001,1.099486847599999900e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -9.169707871 dB, Q=   0.909515200

// [6]
9.476700797749763000e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.476700797749763000e-001,4.633774453999999800e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   6.159407913 dB, Q=   2.158067920

//          N=6
// [12]
2.502256195970254400e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
2.502256195970254400e-001,8.778030991999999900e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -13.280688193 dB, Q=   1.139207643

// [18]
5.828946347579491100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
5.828946347579491100e-001,6.179217705999999800e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -2.137417087 dB, Q=   1.618327833

// [24]
9.696012500246701200e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.696012500246701200e-001,2.303853580000000100e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  12.407805327 dB, Q=   4.340553621

//          N=8
// [30]
1.675357275104121800e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.675357275104121800e-001,7.343526202000000300e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -16.424911926 dB, Q=   1.361743626

// [36]
3.828971949062570700e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
3.828971949062570700e-001,6.005680098000000200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -7.039455011 dB, Q=   1.665090354

// [42]
7.179832678973505600e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
7.179832678973505600e-001,3.885517626000000200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   3.495465831 dB, Q=   2.573659667

// [48]
9.808396549029944100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.808396549029944100e-001,1.378843152000000000e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  16.958601151 dB, Q=   7.252456514

//			N=10
// [54]
1.217698949900869300e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.217698949900869300e-001,6.344129160000000500e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -18.985023525 dB, Q=   1.576260468

// [60]
2.702425127831624100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
2.702425127831624100e-001,5.548108269999999500e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -10.609585710 dB, Q=   1.802416159

// [66]
5.282526963161543500e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
5.282526963161543500e-001,4.283459829999999900e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -1.628701855 dB, Q=   2.334561405

// [72]
8.012496278994487600e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
8.012496278994487600e-001,2.650375649999999700e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   7.671445230 dB, Q=   3.773050058

// [78]
9.869312874637260500e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.869312874637260500e-001,9.180196520000000200e-002,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  20.541565904 dB, Q=  10.893012996

};

/* 	Legendre filters pol locations 
	see PDF document: Optimal_L-Filters_optf2.pdf
	Optimum "L" Filters
	Polynomials, Poles and Circuit Elements
	(c) 2004, C. Bond.
*/

double const Lfilter_Pol_Locations[]=
{
// sigma          +/- j*omega
// ----------------------------
// N=4

 -0.5497434238 , +0.3585718162, // N=4 :[0]
 -0.2316887227 , +0.9455106639, // N=4 :[2]
// N=6:
 -0.4389015496 , +0.2399813521, // N=6 :[4]
 -0.3089608853 , +0.6981674628, // N=6 :[6]
 -0.1151926790 , +0.9779222345, // N=6 :[8]

// N=8:
 -0.3671763101 , +0.1808791995, // N=8 :[10]
 -0.3002840049 , +0.5410422454, // N=8 :[12]
 -0.1942758813 , +0.8247667245, // N=8 :[14]  
 -0.0689421576 , +0.9879709681, // N=8 :[16]

// N=10: 
 -0.3172064580 , +0.1454302513,	// N=10 :[18]
 -0.2774054135 , +0.4396461638,	// N=10 :[20]
 -0.2141729915 , +0.6945377067,	// N=10 :[22]
 -0.1325187825 , +0.8852617693,	// N=10 :[24]
 -0.0459009826 , +0.9923831857,	// N=10 :[26]

};
//---------------------------------------------------

void Lfilter_QFactors(void)
{   double sigma,jomega, bn0,bn1,bn2,an0,an1,an2, g1,q;
    int i;
    
    for (i=0; i <= 26; i=i+2)
    {
        sigma = Lfilter_Pol_Locations[i];
        jomega= Lfilter_Pol_Locations[i+1];
        
        bn2 = 1.0; 
        bn1 = -2*sigma;
        bn0 = sigma*sigma + jomega*jomega;   
        an0 = bn0; 
        an1 = 0.0; 
        an2 = 0.0;
        
        g1 = ((bn0 - bn2)*(bn0 - bn2) + bn1*bn1)/(an0*an0);    // invers betrag quadrat at 1*J
        g1 = 1/g1;
        q  = sqrt(bn2)/bn1;
	eprint("// [%d]\n",i*3);
	eprint("%23.18le,%23.18le,%23.18le, // an0,an1,an2 = \n", an0,an1,an2 );
	eprint("%23.18le,%23.18le,%23.18le, // bn0,bn1,bn2 = \n", bn0,bn1,bn2 );
    eprint("// g1 = %13.9lf dB, Q= %13.9lf\n\n", 10*log10(g1), q);


             
    }
    
}   // Lfilter_QFactors()





FILE	*PLOT;

int main(int argc, char *argv[]) 
{
    int i;
    double db;

	int f1 = 7000;   // plot freq in 0.1 Hz
	int f2 = 8000;

    
if ((FTRACE = fopen( "complex_butter.txt", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "complex_butter.txt" ); return -1; }

eprint("ComplexButterworth Start.\n");

#if 0
for (i=2; i <=12; i=i+2)	// even order
	Build_Butterworth_Filter (& Filter1 [0], Fft_Fs, freq_3db ,750.0, i, 0 );	// prototyp lowpass: print coeffs to file  
eprint("//==========================\n");
#endif

#if 0
eprint("Legendre Filters:\n");
Lfilter_QFactors();	// print only analogue coeffs of Legendrefilters
goto ende;
#endif

// N=2: &Butterworth_SCoeffs[0]     
// N=4: &Butterworth_SCoeffs[6] 
// N=6: &Butterworth_SCoeffs[18] 
// N=8: &Butterworth_SCoeffs[36] 

// N=4:  &LFilter_SCoeffs[0] 
// N=6:  &LFilter_SCoeffs[12] 
// N=8:  &LFilter_SCoeffs[30] 
// N=10: &LFilter_SCoeffs[54]

eprint("2.order Butterworth:\n");
Build_Digital_Filter( & Filter1 [0], Fft_Fs, freq_3db ,750.0, 2, &Butterworth_SCoeffs[0] )	;   // order=2, 20 Hz

if ((PLOT = fopen( "butt2.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "butt2.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);
        
	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);

//-------------------------------------------------------------------


//-------------------------------------------------------------------
#if 0
eprint("2.order staggered :\n");
Build_Digital_Filter( & Filter1 [0], Fft_Fs, freq_3db ,750.0-freq_3db, 2, &Butterworth_SCoeffs[0] )	;   // order=2, 10Hz
Build_Digital_Filter( & Filter2 [0], Fft_Fs, freq_3db ,750.0+freq_3db, 2, &Butterworth_SCoeffs[0] )	;   // order=2, 10Hz

if ((PLOT = fopen( "stagger2.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "stagger2.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0])
        +C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter2 [0]) + 6 ;
        
	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
#endif
//-------------------------------------------------------------------
eprint("Butterw 4.order:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 4, &Butterworth_SCoeffs[6] )	;   // order=4, 10Hz

if ((PLOT = fopen( "butt4.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "butt4.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
eprint("Lfilter 4.order:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift , 4, &LFilter_SCoeffs[0] )	;   // order=4, 10Hz

if ((PLOT = fopen( "legendre4.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "legendre4.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
eprint("6.order Butterw.:\n");
Build_Digital_Filter ( & Filter1 [0],Fft_Fs, freq_3db ,Fshift, 6, &Butterworth_SCoeffs[18] )	;   // order=6, 10Hz

if ((PLOT = fopen( "butt6.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "butt6.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);

//-------------------------------------------------------------------
eprint("8.order Butterw.:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 8, &Butterworth_SCoeffs[36]  )	;   // order=8, 10Hz

if ((PLOT = fopen( "butt8.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "butt8.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
eprint("10.order Butterw.:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 10, &Butterworth_SCoeffs[60]  )	;   // order=10, 10Hz

if ((PLOT = fopen( "butt10.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "butt10.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
//-------------------------------------------------------------------
eprint("12.order Butterw.:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 12, &Butterworth_SCoeffs[90]  )	;   // order=10, 10Hz

if ((PLOT = fopen( "butt12.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "butt12.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------

//-------------------------------------------------------------------
eprint("Lfilter 6.order:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift , 6, &LFilter_SCoeffs[12] )	;   // order=6, 10Hz

if ((PLOT = fopen( "legendre6.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "legendre6.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
//-------------------------------------------------------------------
eprint("8.order Legendre:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 8, &LFilter_SCoeffs[30]  )	;   // order=8, 10Hz

if ((PLOT = fopen( "legendre8.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "legendre8.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
eprint("10.order Legendre:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 10, &LFilter_SCoeffs[54]  )	;   // order=10, 10Hz

if ((PLOT = fopen( "legendre10.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "legendre10.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------

//-------------------------------------------------------------------
eprint("10.order Tschebyscheff equ:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 10, &Tschebyscheff_10equ[0]  )	;   // order=10, 10Hz

if ((PLOT = fopen( "tsch10e.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "tsch10e.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);

//-------------------------------------------------------------------
eprint("10.order Tschebyscheff:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 10, &Tschebyscheff_SCoeffs[144]  )	;   // order=10, 10Hz

if ((PLOT = fopen( "tsch10.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "tsch10.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
//-------------------------------------------------------------------
eprint("9.order ITschebyscheff:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 10, &ITschebyscheff_90dB_SCoeffs[114]  )	;   // order=9, 10Hz

if ((PLOT = fopen( "itsch9.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "itsch9.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
//-------------------------------------------------------------------
eprint("10.order ITschebyscheff:\n");
Build_Digital_Filter ( & Filter1 [0], Fft_Fs, freq_3db ,Fshift, 10, &ITschebyscheff_90dB_SCoeffs[144]  )	;   // order=10, 10Hz

if ((PLOT = fopen( "itsch10.mag", "w"))==NULL)		/* write text */
	{ printf("cannot open: %s\n", "itsch10.mag" ); return -1; }
	
for (i=f1; i <=f2; i++)
{	db = C_Eval_dB ( 0.1 * i, Fft_Fs,& Filter1 [0]);

	fprintf(PLOT,"%lf  %lf \n", 0.1*i - Fshift, db);	/* und hier auf File */
}

fclose(PLOT);
//-------------------------------------------------------------------
ende:
fclose(FTRACE);

	return 0;
}


	
