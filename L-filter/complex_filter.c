//  complex_filter.c        03.04.2014 08:39
//  iir filter implementation


#include <complex.h>

typedef struct Cbiquad_struc_tag
{	int		section;				            // last filter has 0 here
	double	A0,A1,A2,B0,B1,B2,an2,an1,an0,bn2,bn1,bn0 ;	// Coeffs
	
	double _Complex CA0,CA1,CA2,CB1,CB2;        // complex coeffs of difference equation	
	double _Complex CLx_2,CLx_1,CLy_2,CLy_1;	// complex data   of difference equation	

} Cbiquad_struct;


#define BIQUADMAX 4	// 4 biquads for maximal order=8.

Cbiquad_struct	Filter1 [ BIQUADMAX ]; 
Cbiquad_struct	Filter2 [ BIQUADMAX ]; 

/* f1_normed = f1/(Fs/2); normed :  */
/* shift_normed = ((2*M_PI*Fshift)/Fft_Fs); */

void Biquad_construct (	double f1_normed, double shift_normed, Cbiquad_struct * Biq )
{ 	double	c,s,rr,cc,ss,sc ;
    double _Complex lambda = cexp( shift_normed * I);  // e^it

/*                  an0 + an1*s + an2*s^2
	Analog: H(s)=	----------------------
                    bn0 + bn1*s + bn2*s^2


                    A0 + A1*z^-1 + A2*z^-2
	Digital:H(z)=	-----------------------
                    B0 + B1*z^-1 + B2*z^-2	   ; B0 = 1 normed !

*/

/* gamma of Bilinear-Transf.*/

	rr = (M_PI/2)*f1_normed;
	c  = cos(rr); s = sin(rr);		

	/* Bilinear-Transformation ausf�hren:				*/

	cc = c*c; ss = s*s; sc = s*c;

	Biq->A0 = (rr=Biq->an1*sc) + ( Biq->A2= Biq->an2*cc + Biq->an0*ss);
	Biq->A2 = Biq->A2 - rr;
	Biq->A1 = 2*( Biq->an0*ss -Biq->an2*cc);

	Biq->B0 = (rr=Biq->bn1*sc) + ( Biq->B2= Biq->bn2*cc + Biq->bn0*ss);
	Biq->B2 = Biq->B2 - rr;
	Biq->B1 = 2*( Biq->bn0*ss -Biq->bn2*cc);

	Biq->A2 = Biq->A2/Biq->B0; Biq->B2 = Biq->B2/Biq->B0; 
	Biq->A0 = Biq->A0/Biq->B0; Biq->A1 = Biq->A1/Biq->B0;  Biq->B1 = Biq->B1/Biq->B0; 

	Biq->CLx_2 = Biq->CLx_1 = Biq->CLy_2 = Biq->CLy_1 = 0.0*I;	// complex data init Left	

    Biq->CA2 = Biq->A2*lambda*lambda;
    Biq->CB2 = Biq->B2*lambda*lambda; 
    Biq->CA0 = Biq->A0;
    Biq->CA1 = Biq->A1*lambda;
    Biq->CB1 = Biq->B1*lambda;
#if 0
	eprint("\n");
	eprint("A0,A1,A2 = %23.18le,%23.18le,%23.18le\n",Biq->A0,Biq->A1,Biq->A2 );
	eprint("B0,B1,B2 = %23.18le,%23.18le,%23.18le\n",    1.0,Biq->B1,Biq->B2 );
    
    eprint("CA0_re = %23.18le , CA0_im = %23.18le\n", __real__ Biq->CA0, __imag__ Biq->CA0);
    eprint("CA1_re = %23.18le , CA1_im = %23.18le\n", __real__ Biq->CA1, __imag__ Biq->CA1);    
    eprint("CA2_re = %23.18le , CA2_im = %23.18le\n", __real__ Biq->CA2, __imag__ Biq->CA2);
    eprint("CB1_re = %23.18le , CB1_im = %23.18le\n", __real__ Biq->CB1, __imag__ Biq->CB1);    
    eprint("CB2_re = %23.18le , CB2_im = %23.18le\n", __real__ Biq->CB2, __imag__ Biq->CB2);
#endif
				
}	/* Biquad_construct */


//--------- general filter from table:

void Build_Digital_Filter ( Cbiquad_struct * B, double iirFs, double iirfreq_3db, double iirshift, int order, double * anacoeffs )	
{	int i,anz_quads; 

	double fnormed      = (2*iirfreq_3db)/iirFs;
	double shift_normed = ((2*M_PI*iirshift)/iirFs);
			
	anz_quads = order/2;
	if (anz_quads > BIQUADMAX) anz_quads = BIQUADMAX;
	if (anz_quads < 1) anz_quads = 1;		

	for (i=0; i < anz_quads; i++)   // 2. order Polynomfilter
	{		
		B->section = anz_quads-1 -i;
        // read 6 analog coeffs from table:
		B->an0 = *anacoeffs++;
		B->an1 = *anacoeffs++; 
		B->an2 = *anacoeffs++;
		B->bn0 = *anacoeffs++;
		B->bn1 = *anacoeffs++; 
		B->bn2 = *anacoeffs++;		
	
		Biquad_construct (	fnormed, shift_normed, B );	// build coeffs + init data
		B++;
	}
	
}	// Build_Digital_Filter()


double _Complex CFilter_Perform ( double _Complex input, Cbiquad_struct * B )
{	double _Complex result;
	int test;
	
	do
	{   // complex biquad filter operation :
	result = B->CA0* input + B->CA1*B->CLx_1 + B->CA2*B->CLx_2 - B->CB1*B->CLy_1 - B->CB2*B->CLy_2;
	B->CLx_2 = B->CLx_1;
	B->CLx_1 = input;
	B->CLy_2 = B->CLy_1;
	B->CLy_1 = input = result;	// setup input for next stage
	
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return result;				// last cascaded output to caller
	
}	// CFilter_Perform()


//---------------------------------------------------


double Butterworth_SCoeffs[] =
{       
//                          Butterworth Order= 2
// [0]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.414213562373095100e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -3.010299957 dB, Q=   0.707106781

//                          Butterworth Order= 4
// [6]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.847759065022573500e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.332906832 dB, Q=   0.541196100

// [12]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,7.653668647301796700e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   2.322606875 dB, Q=   1.306562965

//                         Butterworth Order= 6
// [18]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.931851652578136600e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.719475475 dB, Q=   0.517638090

// [24]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.414213562373095100e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -3.010299957 dB, Q=   0.707106781

// [30]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,5.176380902050419200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   5.719475475 dB, Q=   1.931851653


//                      Butterworth Order= 8
// [36]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.961570560806460900e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.852078700 dB, Q=   0.509795579

// [42]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.662939224605090500e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -4.417527547 dB, Q=   0.601344887

// [48]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.111140466039204600e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -0.915379284 dB, Q=   0.899976223

// [54]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,3.901806440322566600e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   8.174685575 dB, Q=   2.562915448
};


//---------------------------------------------------

double LFilter_SCoeffs[] =
{       // N=4
// [0]
4.307915793843129500e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
4.307915793843129500e-001,1.099486847599999900e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -9.169707871 dB, Q=   0.909515200

// [6]
9.476700797749763000e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.476700797749763000e-001,4.633774453999999800e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   6.159407913 dB, Q=   2.158067920

//          N=6
// [12]
2.502256195970254400e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
2.502256195970254400e-001,8.778030991999999900e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -13.280688193 dB, Q=   1.139207643

// [18]
5.828946347579491100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
5.828946347579491100e-001,6.179217705999999800e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -2.137417087 dB, Q=   1.618327833

// [24]
9.696012500246701200e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.696012500246701200e-001,2.303853580000000100e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  12.407805327 dB, Q=   4.340553621

//          N=8
// [30]
1.675357275104121800e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.675357275104121800e-001,7.343526202000000300e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -16.424911926 dB, Q=   1.361743626

// [36]
3.828971949062570700e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
3.828971949062570700e-001,6.005680098000000200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -7.039455011 dB, Q=   1.665090354

// [42]
7.179832678973505600e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
7.179832678973505600e-001,3.885517626000000200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   3.495465831 dB, Q=   2.573659667

// [48]
9.808396549029944100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.808396549029944100e-001,1.378843152000000000e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  16.958601151 dB, Q=   7.252456514
};

//---------------------------------------------------

// Init parameters:

// N=2: &Butterworth_SCoeffs[0]     
// N=4: &Butterworth_SCoeffs[6] 
// N=6: &Butterworth_SCoeffs[18] 
// N=8: &Butterworth_SCoeffs[36] 

// N=4: &LFilter_SCoeffs[0] 
// N=6: &LFilter_SCoeffs[12] 
// N=8: &LFilter_SCoeffs[30] 

Build_Digital_Filter( & Filter1 [0], Fft_Fs, 10.0 ,750.0, 2, &Butterworth_SCoeffs[0] )	;   // order=2, 10Hz



