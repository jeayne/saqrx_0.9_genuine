# SAQrx v0.9x genuine

**SAQrx v0.9x** is a soundcard based, Software Defined, [VLF](https://en.wikipedia.org/wiki/Very_low_frequency) receiver based on [release v0.6](https://gitlab.com/jeayne/saqrx_0.6_genuine) developped in 2006/2007 by [Johan H. Bodin](https://github.com/JohBod) (SM6LKM) <br>
and extended in 2012-2014 by [Roland Fröhlich](https://sites.google.com/site/swljo30tb/) (SWL_JO30TB) and [Sabine Cremer](http://dl1dbc.net/) (DL1DBC).

This release uses:
 - [DEV-C++](http://dev-cpp.com/) v5.2: and IDE for C/C++ dev.
 - [PortAudio](http://www.portaudio.com/) v18: an audio I/O library.

The code for v0.94 is only available [here](https://web.archive.org/web/20220619053941/https://sites.google.com/site/swljo30tb).
To preserve this work in a more sustainable place, I'm also publishing it here.

This is NOT my work.

For more information see this [presentation](https://gitlab.com/jeayne/saqrx/-/blob/main/README.md)
