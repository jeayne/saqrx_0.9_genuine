/*	trace.cpp		11.05.2012 04:59
	R.Froehlich
*/
/*****************************************************************************/
/*                 DebugFileTrace : traces to Debug Console                  */  
/*                 show with: DebugView 4.41 sysinternals                    */
/*****************************************************************************/

#include	"saq_config.h"

#ifdef _DEBUG
#include <windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <mmsystem.h>
#include <time.h>
#endif

void DebugFileTrace(char * fmt, ...)
{

#ifdef _DEBUG


static unsigned long trace_starttime_ms = 0;

// static time_t now;	/* C:\Dev-Cpp\include\sys\types.h/27: typedef	long	time_t;  */

unsigned long  currtime;

int cnt;
char buffer[25*80];	/*  Zeilen Buffer, m��te gen�gen */
va_list v;
//-------------

currtime = timeGetTime();		// counting milli-sec after Windows-OS last start

if (trace_starttime_ms == 0)
{	trace_starttime_ms = currtime ;
//	time( &now );                /* Get time as long integer. NOT USED HERE ! */

}

cnt = sprintf(&buffer[0], "SAQrx %09d ms: ", currtime - trace_starttime_ms);

va_start(v, fmt);
cnt = cnt + vsprintf(&buffer[cnt], fmt , v);
va_end(v);

if(cnt)
	if ( buffer[cnt-1]==0x0a) { buffer[cnt-1]=0x0d; buffer[cnt]=0x0a; cnt++; buffer[cnt]=0;} // hyperterminal would be wrong in the meaning of "line-end" !


OutputDebugString (buffer);

#endif	// #ifdef _DEBUG

}	// DebugFileTrace()

