/*  Monitor_Class.h          06.11.2014 07:09

    complex mixer at carrier in RF band, narrow stereo lowpass to DC,
    integrate squared rms amplitude over long time, results to plotter file
    author: Roland Froehlich
    
*/

#ifndef _Monitor_Class_h_
#define _Monitor_Class_h_

#include "saq_config.h"
#include "iir_real_filter.h"

#define datamod2    1024


class Monitor_class
{


private:

    double  station_phaseaccu;
    double  station_PhaseIncr;
    double  station_BW; // Hz bandwidth
    double  statL,statR;
    double  station_summed;
    double  station_add;
    int     integration_periode;  // sample count for one integration period
    int     integration_counter;
     
    Rbiquad_struct	Station_IIR_Filter [ RBIQUADMAX ];




public:	
    int station_number;             // should be 1 for root, 2,3....; inserted from caller.
    double station_env ;
    double zulu_start_time;         // in hours.decimalhours UTC
    double zulu_last_time;          // in hours.decimalhours UTC, last read samples
    double  station_freqHz;
    static int max_block; 			// is same location for all incarnations !

                                    
    Monitor_class *next_monitor;    // pointer to next into linked list, NULL if last in list.
    

    int wr_index, rd_index;         // linear counter to result array:
    double  data_vector[datamod2];  // fifo with modulo access, summed rms2 values over one period

    int pending_lines(void)
    {	if (rd_index >= max_block) return -1;	// end of aquisition !
        else return (wr_index - rd_index); 
    }

	double read_zulutime(void)		// get time at current read position in fifo before read this data item
	{	zulu_last_time = zulu_start_time + ((double)rd_index*(double)integration_periode)/(FS*3600.0);
		return 	zulu_last_time;			
	}
	
	
	double read_dBrms(void)
	{	double val = data_vector[rd_index &(datamod2-1)];
		rd_index++;
		val = val / integration_periode;
		if ( val <= 0.5e-20) val = -200.0;
		else val = 10.0*log10(2*val);		// this was squared rms, compensate mixer loss
		return val;
		
	}
	
	    
	void process_mono_sample (double inputL)
    {   double rms2;

		if (wr_index >= max_block) return;
		
        station_phaseaccu += station_PhaseIncr;
        if (station_phaseaccu > (2.0 * C_PI)) station_phaseaccu -= (2.0 * C_PI);

        // orthogonal mixer :

        statL = inputL * cos (station_phaseaccu);
        statR = inputL * sin (station_phaseaccu);

        // outputs squared stereo filter amplitude:    
        station_add = SquaredStereo_IIR_Perform ( statL, statR, & Station_IIR_Filter[0] ); 
        station_summed += station_add;
        
        if (station_number==1) station_env = sqrt(station_add);  // analytic amplitude

        integration_counter++;
        if (integration_counter >=  integration_periode)
        {   integration_counter = 0;    // counter reset
            rms2 = station_summed ;
            data_vector[ wr_index & (datamod2-1)] = rms2;   // modulo access
            wr_index++;     // linear counter
            station_summed = 0.0;   // sum reset
            
            if (wr_index >= max_block) FTRACE("Monitor #%d: End of processing.\n", station_number);
        }
        

    }   // process_mono_sample
    
    
   
	double integration_timesec;
	
	char callsign[CALLSIGN_MAXLEN+1];	// with terminating 0 char !
	
	char datestring[40];	// date of start	
		
Monitor_class(int stat_num, double integration_seconds, double freqHz, double BW_Hz,int filtertype, char *calls)		// Constructor :
{ 
    station_number = stat_num;    
    next_monitor = NULL;

	integration_timesec = integration_seconds;
		
	callsign[0]=0;	// empty string init	
	strncat (callsign, (const char *) calls, CALLSIGN_MAXLEN );	// copy from caller
    
    integration_periode = (int) (integration_timesec* FS);  // sample count for one integration period
    integration_counter = 0;
    station_summed = 0.0;
    rd_index = wr_index = 0;
    
        
    station_freqHz = freqHz;
    station_BW =  BW_Hz;
       
    // this is a stereo filter, both channels with the same filter coeffs:    
    Build_Real_IIRFilter( & Station_IIR_Filter[0], FS , 0.5*station_BW , Pole_Order(filtertype), AnaProtoLP_Adr(filtertype) );  

    station_phaseaccu = 0.0;
	station_PhaseIncr = (station_freqHz * 2.0 * C_PI / FS);     // set only at start ?

    zulu_start_time = zulu_last_time = ZuluTime_FracHours ( datestring); 


							
}	// Constructor 


~Monitor_class()	// Destructor:
{
    
    
    if (next_monitor) delete next_monitor;      //  recursive deleting

    FTRACE("deleted Monitor_class # %d",station_number);
    
    
//	if (zerobuff) delete zerobuff;
//	if (blockstore) delete blockstore;
//	if (inbuf) delete inbuf;
//	if (INWAV) fclose(INWAV);
					
}	// Destructor
		
//******************************************************************************


};  // Monitor_class

int Monitor_class:: max_block;


#endif // #ifndef _Monitor_Class_h_
