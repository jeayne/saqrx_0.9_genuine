// iir_real_filter.h        03.11.2014 11:43

#ifndef	_iir_real_filter_h
#define	_iir_real_filter_h


#define RBIQUADMAX 4	// 4 real biquads for maximal order=8.

typedef struct Rbiquad_struc_tag
{	int		section;                    // index of section in cascade,last filter has 0 here
	double	an2,an1,an0,bn2,bn1,bn0 ;	// coeffs of analogue prototyp in s-domain
	
	double A0,A1,A2,B0,B1,B2;           // real coeffs of difference equation z-domain, B0=1.0 normed	
	double rLx_2,rLx_1,rLy_2,rLy_1;	    // real data   of difference equation, mono  = Left  data	
	double rRx_2,rRx_1,rRy_2,rRy_1;	    // real data   of difference equation, stereo= Right data	
	
} Rbiquad_struct;

// code in: iir_real_filtercode.h ==>

void Rbiquad_construct (	double f1_normed, Rbiquad_struct * Biq );

double * AnaProtoLP_Adr( int proto_enum);   // map to analogue prototyp lowpass data structure
int Pole_Order( int proto_enum);

void Build_Real_IIRFilter ( Rbiquad_struct * B, double iirFs, double iirfreq_3db, int order, double * anacoeffs );

void Clear_IIR_Filter ( Rbiquad_struct * Biq );

double Mono_IIR_Perform ( double inputL, Rbiquad_struct * B );

double SquaredStereo_IIR_Perform ( double inputL, double inputR, Rbiquad_struct * B );
 // outputs squared stereo filter amplitude
 
//double Real_IIR_Perform ( double input, int iir_select );  // livers single sample output of filter, use FilterIIR[iir_select]


#endif // #ifndef	_iir_real_filter_h_
