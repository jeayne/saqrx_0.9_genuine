/*	saq_config.h		31.12.2014 08:46
	/RFR Roland Froehlich
*/

#ifndef	_saq_config_h	
#define	_saq_config_h



#define VER       "M0.98"	// v or M0.98 for station monitor version
#define version_nr '0.98'	// /SCR nice to have, 4 byte ascii string included in subchunk for wave recorder


#define STATION_MONITOR 1   
// direct conversion (I+Q) of carrier, feed in narrow bandfilters (I+Q) and rms level integrated in long time intervals


//============  CONFIGURATION OF SAMPLERATE ,hardwired  =======>

#define	FS_BASE		48000		// only 44100 or 48000 allowed !
#define	MMSTEPS		24			// 22 (for 44100 etc.) or 24 (for 48000 etc.), major marker steps in kHz until Nyquist
#define	FS_SCALE	1			// 1 or 2 or 4.

//=============================================================<


#define	AF_SCALE	1			// AF Baseband : 1 (or 2 or 4), function of decimation factor DO NOT CHANGE !!!

#define	FS	(FS_SCALE * FS_BASE)    // soundcard sample rate

#define AF_FS   ((double) (AF_SCALE*(FS_BASE/4)))   // Audio band samplerate: 11025 or 12000 Hz

//#define FPB		(256*FS_SCALE)	// requested FPB frames per buffer, let PA determine numBuffers
#define FPB			(512)			// requested FPB frames per buffer, let PA determine numBuffers
//--------------------------------------------------------------------------------------------

#define _DEBUG	0

#define MORSE_SEND		0		// if 1: override input samples and generate a morse code for 17200 Hz(=USB) and LSB.
#define NOISE_ADD		1		// only valid with MORSE_SEND!= 0, make a realistic signal with noise


#define	ASIO_ENVIRONMENT	0	// we are not in an ASIO Host Environment here (this code was ported from an ASIO-Application)		

// only used with debug console:
#if _DEBUG
#define	FTRACE	DebugFileTrace
void DebugFileTrace(char * fmt, ...);
#else
#define	FTRACE
#endif


#define FFT_SIZE_IN   1024    		// time domain input points for the FFT, DO NOT CHANGE !!!


#endif	// _saq_config_h
