//	color.h		21.05.2012 06:27
//	included into main.cpp


const int color_black			= 0x00000000L;	// background for TextOut()
const int color_white			= 0x00FFFFFFL;

//-----------------------------------------------------------------
const int color_spectrum		= 0x0000FF10L;	// middle green for points,lines
const int color_spectrum_hi		= 0x00a6ffacL;  // hi green for points,lines, purpur=0x004B4BFFL;
const int color_spectrum_audio	= 0x00FFFF00L;	// light blue for audio fft
const int color_tuningbox		= 0x00408080L;	// oliv for rectangle in fft tuning
//-----------------------------------------------------------------

const int color_RMS_bar			= 0x004B4BFFL;	// decent red
const int color_gain_text		= 0x004B4BFFL;	// red 0x0000FFFFL;	// middle yellow
const int color_gain_inactiv	= 0x00808080L;	// greyed out
//-----------------------------------------------------------------

const int color_VFO_bar			= 0x0000FFFFL;	// middle yellow 0x000000FFL;	// red in spectrum area
const int color_VFO_text		= 0x0000FFFFL;	// middle yellow 0x000000FFL;	// red
//-----------------------------------------------------------------

const int color_filterBW_text	= 0x00FFFF00L;	// light blue & audio fft
const int color_filterBW_bar 	= 0x00808000L;	// dark  blue in spectrum area
//-----------------------------------------------------------------


const int color_help_box		= 0x00A07000L;	// dark blue
const int color_help_text		= 0x00F0C080L;	// light greyed blue
//-----------------------------------------------------------------
const int color_tapetime_idle 	= 0x00F0C080L;
const int color_tapetime_mono 	= 0x0080FFFFL;
const int color_tapetime_stereo = 0x008000FFL;
//-----------------------------------------------------------------
const int color_dB_numbers 		= 0x0000C4C4L;	// dark yellow
const int color_dB_ticks 		= 0x0000C4C4L;	// dark yellow
const int color_dB_unit 		= 0x0080FFFFL;	// bright yellow

const int color_kHz_numbers 	= 0x0000C4C4L;	// dark yellow
//const int color_kHz_ticks 	= 0x0000C4C4L;	// dark yellow
const int color_kHz_unit 		= 0x0080FFFFL;	// bright yellow

const int color_rastergrid 		= 0x00404040L;	// dark gray dotted raster
const int color_fft_frame 		= 0x00404040L;	// dark gray frame for fft

//-----------------------------------------------------------------
const int color_overdrive_text 	= 0x000000FFL;	// red 
const int color_pure_red		= 0x000000FFL;	// red 

const int color_status_text		= 0x00808080L;	// grey for CPU% etc. in status line
const int color_status_clock	= 0x00F0C080L;	// light greyed blue
//-----------------------------------------------------------------


