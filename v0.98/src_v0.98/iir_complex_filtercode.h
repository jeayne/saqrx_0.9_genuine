//  iir_complex_filtercode.h        23.11.2014 15:27
//  iir filter implementation


#include <complex.h>

#include "iir_complex_filter.h"

//#include "Tschebyscheff_Coeff.h"


// Legendre, Butterworth, Tschebyscheff (0.25dB pp-ripple) filters, order 2 .. 20.
// Frequency Response ( -3dB at BW) at octave = 2*BW :
// L2   =  -12.30 dB.  B2  =   -12.30 dB.     T2   =  -13.69 dB.   
// L3   =  -21.73 dB.  B3  =   -18.13 dB.     T3   =  -22.62 dB. 
// L4   =  -30.31 dB.  B4  =   -24.10 dB.     T4   =  -32.61 dB. 
// L5   =  -40.76 dB.  B5  =   -30.11 dB.     T5   =  -43.11 dB. 
// L6   =  -50.27 dB.  B6  =   -36.12 dB.     T6   =  -53.89 dB. 
// L7   =  -61.01 dB.  B7  =   -42.14 dB.     T7   =  -64.84 dB. 
// L8   =  -70.98 dB.  B8  =   -48.16 dB.     T8   =  -75.91 dB. 
// L9   =  -81.88 dB.  B9  =   -54.19 dB.     T9   =  -87.06 dB. 
// L10  =  -92.13 dB.  B10 =   -60.21 dB.     T10  =  -98.27 dB. 
// L11  = -103.13 dB.  B11 =   -66.23 dB.     T11  = -109.51 dB. 
// L12  = -113.57 dB.  B12 =   -72.25 dB.     T12  = -120.79 dB. 
// L13  = -124.64 dB.  B13 =   -78.27 dB.     T13  = -132.09 dB. 
// L14  = -135.22 dB.  B14 =   -84.29 dB.     T14  = -143.41 dB. 
// L15  = -146.33 dB.  B15 =   -90.31 dB.     T15  = -154.75 dB. 
// L16  = -157.02 dB.  B16 =   -96.33 dB.     T16  = -166.09 dB. 
// L17  = -168.17 dB.  B17 =  -102.35 dB.     T17  = -177.45 dB. 
// L18  = -178.94 dB.  B18 =  -108.37 dB.     T18  = -188.82 dB. 
// L19  = -190.12 dB.  B19 =  -114.39 dB.     T19  = -200.20 dB. 
// L20  = -200.95 dB.  B20 =  -120.41 dB.     T20  = -211.58 dB. 
// this list was computed with PARI/LFilter.gp 14.12.2014 18:35


#define BIQUADMAX 6	// 6 biquads for maximal order= 12.

Cbiquad_struct	FilterX[10] [ BIQUADMAX ]; // 0 =FIR, not used; 1-9 for iir type

/* f1_normed = f1/(Fs/2); normed :  */
/* shift_normed = ((2*M_PI*Fshift)/Fft_Fs); */

void CBiquad_construct (	double f1_normed, double shift_normed, Cbiquad_struct * Biq )
{ 	double	c,s,rr,cc,ss,sc ;
    double _Complex lambda = cexp( shift_normed * I);  // e^it

/*                  an0 + an1*s + an2*s^2
	Analog: H(s)=	----------------------
                    bn0 + bn1*s + bn2*s^2


                    A0 + A1*z^-1 + A2*z^-2
	Digital:H(z)=	-----------------------
                    B0 + B1*z^-1 + B2*z^-2	   ; B0 = 1 normed !

*/

/* gamma of Bilinear-Transf.*/

	rr = (M_PI/2)*f1_normed;
	c  = cos(rr); s = sin(rr);		

	/* Bilinear-Transformation ausf�hren:				*/

	cc = c*c; ss = s*s; sc = s*c;

	Biq->A0 = (rr=Biq->an1*sc) + ( Biq->A2= Biq->an2*cc + Biq->an0*ss);
	Biq->A2 = Biq->A2 - rr;
	Biq->A1 = 2*( Biq->an0*ss -Biq->an2*cc);

	Biq->B0 = (rr=Biq->bn1*sc) + ( Biq->B2= Biq->bn2*cc + Biq->bn0*ss);
	Biq->B2 = Biq->B2 - rr;
	Biq->B1 = 2*( Biq->bn0*ss -Biq->bn2*cc);

	Biq->A2 = Biq->A2/Biq->B0; Biq->B2 = Biq->B2/Biq->B0; 
	Biq->A0 = Biq->A0/Biq->B0; Biq->A1 = Biq->A1/Biq->B0;  Biq->B1 = Biq->B1/Biq->B0; 

	Biq->CLx_2 = Biq->CLx_1 = Biq->CLy_2 = Biq->CLy_1 = 0.0*I;	// complex data init Left	

    Biq->CA2 = Biq->A2*lambda*lambda;
    Biq->CB2 = Biq->B2*lambda*lambda; 
    Biq->CA0 = Biq->A0;
    Biq->CA1 = Biq->A1*lambda;
    Biq->CB1 = Biq->B1*lambda;
#if 0
	eprint("\n");
	eprint("A0,A1,A2 = %23.18le,%23.18le,%23.18le\n",Biq->A0,Biq->A1,Biq->A2 );
	eprint("B0,B1,B2 = %23.18le,%23.18le,%23.18le\n",    1.0,Biq->B1,Biq->B2 );
    
    eprint("CA0_re = %23.18le , CA0_im = %23.18le\n", __real__ Biq->CA0, __imag__ Biq->CA0);
    eprint("CA1_re = %23.18le , CA1_im = %23.18le\n", __real__ Biq->CA1, __imag__ Biq->CA1);    
    eprint("CA2_re = %23.18le , CA2_im = %23.18le\n", __real__ Biq->CA2, __imag__ Biq->CA2);
    eprint("CB1_re = %23.18le , CB1_im = %23.18le\n", __real__ Biq->CB1, __imag__ Biq->CB1);    
    eprint("CB2_re = %23.18le , CB2_im = %23.18le\n", __real__ Biq->CB2, __imag__ Biq->CB2);
#endif
				
}	/* CBiquad_construct */


//--------- general filter from table:

void Build_Complex_IIRFilter ( Cbiquad_struct * B, double iirFs, double iirfreq_3db, double iirshift, int order, double * anacoeffs )	
{	int i,anz_quads; 

	double fnormed      = (2*iirfreq_3db)/iirFs;
	double shift_normed = ((2*M_PI*iirshift)/iirFs);
			
	anz_quads = order/2;
	if (anz_quads > BIQUADMAX) anz_quads = BIQUADMAX;
	if (anz_quads < 1) anz_quads = 1;		

	for (i=0; i < anz_quads; i++)   // 2. order Polynomfilter
	{		
		B->section = anz_quads-1 -i;
        // read 6 analog coeffs from table:
		B->an0 = *anacoeffs++;
		B->an1 = *anacoeffs++; 
		B->an2 = *anacoeffs++;
		B->bn0 = *anacoeffs++;
		B->bn1 = *anacoeffs++; 
		B->bn2 = *anacoeffs++;		
	
		CBiquad_construct (	fnormed, shift_normed, B );	// build coeffs + init data
		B++;
	}
	
}	// Build_Complex_IIRFilter()


double _Complex CFilter_Perform ( double _Complex input, Cbiquad_struct * B )
{	double _Complex result;
	int test;
	
	do
	{   // complex biquad filter operation :
	result = B->CA0* input + B->CA1*B->CLx_1 + B->CA2*B->CLx_2 - B->CB1*B->CLy_1 - B->CB2*B->CLy_2;
	B->CLx_2 = B->CLx_1;
	B->CLx_1 = input;
	B->CLy_2 = B->CLy_1;
	B->CLy_1 = input = result;	// setup input for next stage
	
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return result;				// last cascaded output to caller
	
}	// CFilter_Perform()


double _Complex IQ_IIR_Perform ( double _Complex input, int iirtype )
{	double _Complex result;
    Cbiquad_struct * B = & FilterX[iirtype] [0];
	int test;
	
	do
	{   // complex biquad filter operation :
	result = B->CA0* input + B->CA1*B->CLx_1 + B->CA2*B->CLx_2 - B->CB1*B->CLy_1 - B->CB2*B->CLy_2;
	B->CLx_2 = B->CLx_1;
	B->CLx_1 = input;
	B->CLy_2 = B->CLy_1;
	B->CLy_1 = input = result;	// setup input for next stage
	
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return result;				// last cascaded output to caller
	
}	// IQ_IIR_Perform()


//---------------------------------------------------

// Butterworth_Coeff.h      23.11.2014 09:55
// analogue prototyp lowpass, normed at s= j*1 for -3dB point.

// Butterworth Order= 2 , coeffs at &Butterworth_SCoeffs[0]
// Butterworth Order= 4 , coeffs at &Butterworth_SCoeffs[6]
// Butterworth Order= 6 , coeffs at &Butterworth_SCoeffs[18]
// Butterworth Order= 8 , coeffs at &Butterworth_SCoeffs[36]
// Butterworth Order= 10, coeffs at &Butterworth_SCoeffs[60]
// Butterworth Order= 12, coeffs at &Butterworth_SCoeffs[90]

// until order N=12:
double Butterworth_SCoeffs[] =
{ 
// Butterworth Order= 2, coeffs at &Butterworth_SCoeffs[0]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.414213562373095100e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -3.010299957 dB, Q=   0.707106781

// Butterworth Order= 4, coeffs at &Butterworth_SCoeffs[6]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.847759065022573500e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.332906832 dB, Q=   0.541196100

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,7.653668647301796700e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   2.322606875 dB, Q=   1.306562965

// Butterworth Order= 6, coeffs at &Butterworth_SCoeffs[18]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.931851652578136600e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.719475475 dB, Q=   0.517638090

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.414213562373095100e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -3.010299957 dB, Q=   0.707106781

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,5.176380902050419200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   5.719475475 dB, Q=   1.931851653

// Butterworth Order= 8, coeffs at &Butterworth_SCoeffs[36]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.961570560806460900e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.852078700 dB, Q=   0.509795579

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.662939224605090500e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -4.417527547 dB, Q=   0.601344887

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.111140466039204600e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -0.915379284 dB, Q=   0.899976223

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,3.901806440322566600e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   8.174685575 dB, Q=   2.562915448

// Butterworth Order= 10, coeffs at &Butterworth_SCoeffs[60]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.975376681190275500e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.912998455 dB, Q=   0.506232563

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.782013048376735800e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.018217595 dB, Q=   0.561163119

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.414213562373095100e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -3.010299957 dB, Q=   0.707106781

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,9.079809994790936100e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   0.838464790 dB, Q=   1.101344632

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,3.128689300804618500e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  10.092751260 dB, Q=   3.196226611

// Butterworth Order= 12, coeffs at &Butterworth_SCoeffs[90]
1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.982889722747620800e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.945971237 dB, Q=   0.504314480

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.847759065022573500e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -5.332906832 dB, Q=   0.541196100

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.586706680582470600e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -4.009933005 dB, Q=   0.630236207

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,1.217522858017441300e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -1.709542470 dB, Q=   0.821339816

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,7.653668647301796700e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   2.322606875 dB, Q=   1.306562965

1.000000000000000000e+000,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.000000000000000000e+000,2.610523844401034200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  11.665446712 dB, Q=   3.830648788
//========================================
};  // double Butterworth_SCoeffs[]



//---------------------------------------------------
// until order N=10:
double LFilter_SCoeffs[] =
{       // N=4
// [0]
4.307915793843129500e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
4.307915793843129500e-001,1.099486847599999900e+000,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -9.169707871 dB, Q=   0.909515200

// [6]
9.476700797749763000e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.476700797749763000e-001,4.633774453999999800e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   6.159407913 dB, Q=   2.158067920

//          N=6
// [12]
2.502256195970254400e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
2.502256195970254400e-001,8.778030991999999900e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -13.280688193 dB, Q=   1.139207643

// [18]
5.828946347579491100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
5.828946347579491100e-001,6.179217705999999800e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -2.137417087 dB, Q=   1.618327833

// [24]
9.696012500246701200e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.696012500246701200e-001,2.303853580000000100e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  12.407805327 dB, Q=   4.340553621

//          N=8
// [30]
1.675357275104121800e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.675357275104121800e-001,7.343526202000000300e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -16.424911926 dB, Q=   1.361743626

// [36]
3.828971949062570700e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
3.828971949062570700e-001,6.005680098000000200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -7.039455011 dB, Q=   1.665090354

// [42]
7.179832678973505600e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
7.179832678973505600e-001,3.885517626000000200e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   3.495465831 dB, Q=   2.573659667

// [48]
9.808396549029944100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.808396549029944100e-001,1.378843152000000000e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  16.958601151 dB, Q=   7.252456514

//			N=10
// [54]
1.217698949900869300e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
1.217698949900869300e-001,6.344129160000000500e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -18.985023525 dB, Q=   1.576260468

// [60]
2.702425127831624100e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
2.702425127831624100e-001,5.548108269999999500e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 = -10.609585710 dB, Q=   1.802416159

// [66]
5.282526963161543500e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
5.282526963161543500e-001,4.283459829999999900e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  -1.628701855 dB, Q=   2.334561405

// [72]
8.012496278994487600e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
8.012496278994487600e-001,2.650375649999999700e-001,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =   7.671445230 dB, Q=   3.773050058

// [78]
9.869312874637260500e-001,0.000000000000000000e+000,0.000000000000000000e+000, // an0,an1,an2 = 
9.869312874637260500e-001,9.180196520000000200e-002,1.000000000000000000e+000, // bn0,bn1,bn2 = 
// g1 =  20.541565904 dB, Q=  10.893012996

};

//---------------------------------------------------

// Init parameters:

// N=2: &Butterworth_SCoeffs[0]     
// N=4: &Butterworth_SCoeffs[6] 
// N=6: &Butterworth_SCoeffs[18] 
// N=8: &Butterworth_SCoeffs[36] 

// N=4:  &LFilter_SCoeffs[0] 
// N=6:  &LFilter_SCoeffs[12] 
// N=8:  &LFilter_SCoeffs[30]
// N=10: &LFilter_SCoeffs[54] 

// Build_Complex_IIRFilter( & Filter1 [0], AF_FS , 10.0 ,750.0, 2, &Butterworth_SCoeffs[0] )	;   // order=2, 10Hz



