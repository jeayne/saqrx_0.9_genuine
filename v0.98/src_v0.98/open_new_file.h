/*	open_new_file.h		25.11.2014 16:29

	Roland Froehlich			

version history:
---------------------------------------------------------------------------
25.11.2014  full path of exe-directory with currdir constructed
30.01.2008	filetime stamp in UTC, GetSystemTime instead of GetLocalTime

---------------------------------------------------------------------------
*/

int open_new_binfile( FILE * *Fp,  const char * path_prefix, const char * lower_extension , char * resultname, char * date_time_txt, int zulu0 );

void open_new_textfile( FILE * *Fp,  const char * path_prefix, const char * lower_extension , char * resultname );

/*

typedef struct _SYSTEMTIME 
{ 
    WORD wYear; 
    WORD wMonth; 
    WORD wDayOfWeek; 
    WORD wDay; 
    WORD wHour; 
    WORD wMinute; 
    WORD wSecond; 
    WORD wMilliseconds; 
} SYSTEMTIME; 

VOID GetLocalTime( LPSYSTEMTIME lpSystemTime );  // address of system time structure 

year : ABC...
month: ABCDEFGHIJKL
day  : 01..31
hour : 00..23
min  : 00..59
sec  : 00..59

*/

char * Monat_Txt (int Month)
{	
	switch (Month)
	{
		case 1:		return "JAN";
		case 2:		return "FEB";
		case 3:		return "MAR";
		case 4:		return "APR";
		case 5:		return "MAY";
		case 6:		return "JUN";
		case 7:		return "JUL";
		case 8:		return "AUG";
		case 9:		return "SEP";
		case 10:	return "OCT";
		case 11:	return "NOV";
		case 12:	return "DEC";		
	}

	return "Mm?";	
}

#if 0
void Latch_zulu_datetime_string(  char * date_time_txt)
{
	SYSTEMTIME	st;
	GetSystemTime( & st);
	
	long Year		    = (long) st.wYear;                
    long Month          = (long) st.wMonth;             
    long Day            = (long) st.wDay;               
    long Hour           = (long) st.wHour;              
    long Minute         = (long) st.wMinute;            
    long Second         = (long) st.wSecond;   
//  long mSecond        = (long) st.wMilliseconds;
    
	if ( date_time_txt != NULL)
		sprintf(date_time_txt, "%02d%s%04dz%02d%02d%02d", Day,Monat_Txt(Month),Year, Hour,Minute,Second );

}	// Latch_zulu_datetime_string()
#endif

double ZuluTime_FracHours (char * datestring)    // returns UTC 0.000 ... 23.999 als hours.decimalfraction, date to string
{   double hfrac;

    SYSTEMTIME	st;	
    GetSystemTime( &st);  
    
	long Year		    = (long) st.wYear;                
    long Month          = (long) st.wMonth;             
    long Day            = (long) st.wDay;         

    long Hour           = (long) st.wHour;              
    long Minute         = (long) st.wMinute;            
    long Second         = (long) st.wSecond;   
	long mSecond		= (long) st.wMilliseconds;    	    

	if ( datestring != NULL)
		sprintf(datestring, "%02d%s%04d_%02d%02d%02dz", Day,Monat_Txt(Month),Year, Hour,Minute,Second );

    hfrac =  Minute/60.0 + (Second + 0.001*mSecond)/3600.0;
    return (Hour + hfrac);   
}



int open_new_binfile( FILE * *Fp,  const char * path_prefix, const char * lower_extension , char * resultname , char * date_time_txt, int zulu0)
{
	char actname[260+512];
	SYSTEMTIME	st;	
	char abc;
	
	if (zulu0==0)
// we will conform to a timestamp understand in all the world, and use the UTC time here:
// in UTC (~GMT) : reference to the same detected events (approximativly with c propagated) on our earth :
	{ GetSystemTime( &st); abc='z'; }	
	else
//  MS-Windows assumed this time zone - the time on your home clock in your country :
	{ GetLocalTime( & st); abc='L'; }

	
	long Year		    = (long) st.wYear;                
    long Month          = (long) st.wMonth;             
    long Day            = (long) st.wDay;               
    long Hour           = (long) st.wHour;              
    long Minute         = (long) st.wMinute;            
    long Second         = (long) st.wSecond;   
	long mSecond		= (long) st.wMilliseconds;

	if ( date_time_txt != NULL)
		sprintf(date_time_txt, "%02d-%s-%04d %02d:%02d:%02d", Day,Monat_Txt(Month),Year, Hour,Minute,Second );
    
	*Fp = NULL;	/* default invalid FILE pointer to caller */

	
	sprintf(actname, "%s\\%s_%02d%s%04d_%02d%02d%02d%c%03d_%s", currdir,path_prefix,Day,Monat_Txt(Month),Year, Hour,Minute,Second,abc,mSecond ,lower_extension);	
                 	
	*Fp = fopen( actname , "wb") ;	/* open file for write binary  , FILE pointer to caller */

	if (resultname)		
	strcpy( resultname, actname );	// name to caller	

	if (*Fp == NULL)
	{	
//		MessageBox (NULL, "Write File not possible !","       SAQrx Warning ", MB_ICONEXCLAMATION | MB_OK);	
		return 1;	// no_retry = 1;
	}
	else
		return 0;	// no_retry = 0;
			
}	// open_new_binfile()	


void open_new_textfile( FILE * *Fp,  const char * path_prefix, const char * lower_extension , char * resultname )
{
	char actname[260+512];
	SYSTEMTIME	st;
	GetLocalTime( & st);
	
	long Year		    = (long) st.wYear;                
    long Month          = (long) st.wMonth;             
    long Day            = (long) st.wDay;               
    long Hour           = (long) st.wHour;              
    long Minute         = (long) st.wMinute;            
    long Second         = (long) st.wSecond;   
    
	*Fp = NULL;	/* default invalid FILE pointer to caller */

//	Year = Year - 2000;
//	if (Year < 0) Year = 0;
	
//	sprintf(actname , "%s%02d%02d%02d_%02d%02d%02d%s", path_prefix, Year,Month,Day,Hour,Minute,Second,lower_extension);

	sprintf(actname, "%s-%02d%s%04d-%02d%02d%02d%s", path_prefix,Day,Monat_Txt(Month),Year, Hour,Minute,Second ,lower_extension);	

	*Fp = fopen( actname , "w") ;	/* open file for write text  , FILE pointer to caller */
		
	strcpy( resultname, actname );	// name to caller	
	
}	// open_new_textfile()	


int get_UTC( char * time_txt)	// returned char counter
{
	SYSTEMTIME	st;	

//#if	TIMEZONE_UTC
	// we will conform to a timestamp understand in all the world, and use the UTC time here:
	// in UTC (~GMT) : reference to the same detected events (approximativly with c propagated) on our earth :
	GetSystemTime( &st);	
//#else
	//  MS-Windows assumed this time zone - the time on your home clock in your country :
//	GetLocalTime( & st);
//#endif
	
	long Year		    = (long) st.wYear;                
    long Month          = (long) st.wMonth;             
    long Day            = (long) st.wDay;               
    long Hour           = (long) st.wHour;              
    long Minute         = (long) st.wMinute;            
    long Second         = (long) st.wSecond;   
	long mSecond		= (long) st.wMilliseconds;

//	if ( time_txt != NULL)
	return	sprintf( time_txt, "%02d:%02d:%02d UTC", Hour,Minute,Second );

} // get_UTC

int get_datetimeZ( char * time_txt, int zulu0)	// returned char counter, zulu0=0 for UTC, else local time
{
	SYSTEMTIME	st;	

	if (zulu0 == 0) GetSystemTime( &st);	
	else			GetLocalTime ( &st);
	
	long Year		    = (long) st.wYear;                
    long Month          = (long) st.wMonth;             
    long Day            = (long) st.wDay;               
    long Hour           = (long) st.wHour;              
    long Minute         = (long) st.wMinute;            
    long Second         = (long) st.wSecond;   
	long mSecond		= (long) st.wMilliseconds;

	if (zulu0 == 0)
		return	sprintf( time_txt, "%2d-%s-%4d UTC %02d:%02d:%02d", Day,Monat_Txt(Month),Year, Hour,Minute,Second );
	else
		return	sprintf( time_txt, "%2d-%s-%4d %02d:%02d:%02d", Day,Monat_Txt(Month),Year, Hour,Minute,Second );
		
}	// get_datetimeZ()

int get_UTC_date( char * time_txt)	// returned char counter
{
	SYSTEMTIME	st;	

//#if	TIMEZONE_UTC
	// we will conform to a timestamp understand in all the world, and use the UTC time here:
	// in UTC (~GMT) : reference to the same detected events (approximativly with c propagated) on our earth :
	GetSystemTime( &st);	
//#else
	//  MS-Windows assumed this time zone - the time on your home clock in your country :
//	GetLocalTime( & st);
//#endif
	
	long Year		    = (long) st.wYear;                
    long Month          = (long) st.wMonth;             
    long Day            = (long) st.wDay;               
    long Hour           = (long) st.wHour;              
    long Minute         = (long) st.wMinute;            
    long Second         = (long) st.wSecond;   
	long mSecond		= (long) st.wMilliseconds;

//	if ( time_txt != NULL)
	return	sprintf( time_txt, "%2d-%s-%4d UTC %02d:%02d:%02d", Day,Monat_Txt(Month),Year, Hour,Minute,Second );

} // get_UTC_date




