//	coeffs.h		23.11.2014 16:01
//	R.Fr�hlich

#ifndef COEFFS_INCLUDED
#define COEFFS_INCLUDED


#define BUTTERWORTH_2POL    1
#define BUTTERWORTH_4POL    2
#define BUTTERWORTH_6POL    3
#define BUTTERWORTH_8POL    4
#define BUTTERWORTH_10POL   5
#define BUTTERWORTH_12POL   6
#define LEGENDRE_4POL       7
#define LEGENDRE_6POL       8
#define LEGENDRE_8POL       9

// IP16_AM has L= 432 ==>
#define DECI_DATALEN	512		// for modulo addressing, must be power of 2 >= maximal TAP number of Decimation Filters
#define INTP_DATALEN 	128		// """, per phase of interpolation, 2^n >= TAPS/IPfactor

typedef struct Filter_typ_tag 
{
	int  	IP_Factor;				// interpolation or decimation factor,typical = 4/8/16, or 1 for IQ-filters (no meaning)			
    int  	All_Taps;				// number of used taps       
    float	FilterCoeffs[1500];		// my maximal taps, coeffs are defined for index = [0] ..  [All_Taps-1]        
} Filter_typ, * Ptr_Filter_typ ;

#define FIR_LARGEST_POW2	2048	// next 2^n of maximal tap count 1500, for modulo addressing

void Filters_Init (void);			// located in coeffs.c
void FilterIQ_Set (int casus);		// located in coeffs.c
void vQSY (float rOffset);			// in main: Change LO frequency by Offset and check range !

enum
{
	filt_CW50,		// = 0
	filt_CW100,
	filt_CW300,
	filt_CW1000, filt_CWmax=filt_CW1000,
	filt_SSB,
	filt_AM,	 filt_MAX=filt_AM,		// until here: range of filter_case

	filt_IPxx_AM,		   
	filt_IPxx_SSB,       
	filt_IPxx_CW, 

};



#endif  // COEFFS_INCLUDED
