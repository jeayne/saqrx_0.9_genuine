// iir_complex_filter.h

typedef struct Cbiquad_struc_tag
{	int		section;				            // index of section in cascade,last filter has 0 here
	double	A0,A1,A2,B0,B1,B2,an2,an1,an0,bn2,bn1,bn0 ;	// Coeffs
	
	double _Complex CA0,CA1,CA2,CB1,CB2;        // complex coeffs of difference equation	
	double _Complex CLx_2,CLx_1,CLy_2,CLy_1;	// complex data   of difference equation	

} Cbiquad_struct;

double _Complex IQ_IIR_Perform ( double _Complex input, int iirtype );

