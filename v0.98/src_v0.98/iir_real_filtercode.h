//  iir_real_filtercode.h        23.11.2014 16:00
//  iir filter implementation

#ifndef	_iir_real_filtercode_h_
#define _iir_real_filtercode_h_

#include "iir_real_filter.h"

double * AnaProtoLP_Adr( int proto_enum)
{
    switch(proto_enum)
    {
        case  BUTTERWORTH_2POL: return &Butterworth_SCoeffs[ 0]; break;
        case  BUTTERWORTH_4POL: return &Butterworth_SCoeffs[ 6]; break;        
        case  BUTTERWORTH_6POL: return &Butterworth_SCoeffs[18]; break;        
        case  BUTTERWORTH_8POL: return &Butterworth_SCoeffs[36]; break;  
        case  BUTTERWORTH_10POL: return &Butterworth_SCoeffs[60]; break;        
        case  BUTTERWORTH_12POL: return &Butterworth_SCoeffs[90]; break;                
        case  LEGENDRE_4POL:    return &LFilter_SCoeffs[ 0]; break;  
        case  LEGENDRE_6POL:    return &LFilter_SCoeffs[12]; break;          
        case  LEGENDRE_8POL:    return &LFilter_SCoeffs[30]; break;  
        default:                return &Butterworth_SCoeffs[ 0];                     
    }    
}

int Pole_Order( int proto_enum)
{
    switch(proto_enum)
    {
        case  BUTTERWORTH_2POL: return 2; //&Butterworth_SCoeffs[ 0]; break;
        case  BUTTERWORTH_4POL: return 4; //&Butterworth_SCoeffs[ 6]; break;        
        case  BUTTERWORTH_6POL: return 6; //&Butterworth_SCoeffs[18]; break;        
        case  BUTTERWORTH_8POL: return 8; //&Butterworth_SCoeffs[36]; break;        
        case  BUTTERWORTH_10POL: return 10; //&Butterworth_SCoeffs[60]; break;        
        case  BUTTERWORTH_12POL: return 12; //&Butterworth_SCoeffs[90]; break;  
        case  LEGENDRE_4POL:    return 4; //&LFilter_SCoeffs[ 0]; break;  
        case  LEGENDRE_6POL:    return 6; //&LFilter_SCoeffs[12]; break;          
        case  LEGENDRE_8POL:    return 8; //&LFilter_SCoeffs[30]; break;  
        default:                return 2; //&Butterworth_SCoeffs[ 0];                     
    }    
}

/* f1_normed = f1/(Fs/2); normed :  */

void Rbiquad_construct ( double f1_normed, Rbiquad_struct * Biq )
{ 	double	c,s,rr,cc,ss,sc ;

/*                  an0 + an1*s + an2*s^2
	Analog: H(s)=	----------------------
                    bn0 + bn1*s + bn2*s^2


                    A0 + A1*z^-1 + A2*z^-2
	Digital:H(z)=	-----------------------
                    B0 + B1*z^-1 + B2*z^-2	   ; B0 = 1 normed !

*/

/* gamma of Bilinear-Transf.*/

	rr = (M_PI/2)*f1_normed;
	c  = cos(rr); s = sin(rr);		

	/* Bilinear-Transformation ausf�hren:				*/

	cc = c*c; ss = s*s; sc = s*c;

	Biq->A0 = (rr=Biq->an1*sc) + ( Biq->A2= Biq->an2*cc + Biq->an0*ss);
	Biq->A2 = Biq->A2 - rr;
	Biq->A1 = 2*( Biq->an0*ss -Biq->an2*cc);

	Biq->B0 = (rr=Biq->bn1*sc) + ( Biq->B2= Biq->bn2*cc + Biq->bn0*ss);
	Biq->B2 = Biq->B2 - rr;
	Biq->B1 = 2*( Biq->bn0*ss -Biq->bn2*cc);

	Biq->A2 = Biq->A2/Biq->B0; Biq->B2 = Biq->B2/Biq->B0; 
	Biq->A0 = Biq->A0/Biq->B0; Biq->A1 = Biq->A1/Biq->B0;  Biq->B1 = Biq->B1/Biq->B0; 

	Biq->rLx_2 = Biq->rLx_1 = Biq->rLy_2 = Biq->rLy_1 = 0.0;	// real data init Left	
	Biq->rRx_2 = Biq->rRx_1 = Biq->rRy_2 = Biq->rRy_1 = 0.0;	// real data init Right	
	
#if 1
	FTRACE("\n");
	FTRACE("A0,A1,A2 = %23.18le,%23.18le,%23.18le\n",Biq->A0,Biq->A1,Biq->A2 );
	FTRACE("B0,B1,B2 = %23.18le,%23.18le,%23.18le\n",    1.0,Biq->B1,Biq->B2 );
#endif
				
}	/* Rbiquad_construct */


//--------- general filter from table:

void Build_Real_IIRFilter ( Rbiquad_struct * B, double iirFs, double iirfreq_3db, int order, double * anacoeffs )	
{	int i,anz_quads; 

	double fnormed      = (2*iirfreq_3db)/iirFs;
			
	anz_quads = order/2;
	if (anz_quads > RBIQUADMAX) anz_quads = RBIQUADMAX;
	if (anz_quads < 1) anz_quads = 1;		

	for (i=0; i < anz_quads; i++)   // 2. order Polynomfilter
	{		
		B->section = anz_quads-1 -i;
        // read 6 analog coeffs from table:
		B->an0 = *anacoeffs++;
		B->an1 = *anacoeffs++; 
		B->an2 = *anacoeffs++;
		B->bn0 = *anacoeffs++;
		B->bn1 = *anacoeffs++; 
		B->bn2 = *anacoeffs++;		
	
		Rbiquad_construct (	fnormed, B );	// build coeffs + init data
		B++;
	}
	
}	// Build_Real_IIRFilter()

void Clear_IIR_Filter ( Rbiquad_struct * Biq )
{   int test;
	
	do
	{   	    	    
    	Biq->rLx_2 = Biq->rLx_1 = Biq->rLy_2 = Biq->rLy_1 = 0.0;	// real data init Left	
    	Biq->rRx_2 = Biq->rRx_1 = Biq->rRy_2 = Biq->rRy_1 = 0.0;	// real data init Right	
	
    	if ((test=Biq->section) > 0) Biq++;
	
	} while (test > 0);
}   // Clear_IIR_Filter()


double Mono_IIR_Perform ( double inputL, Rbiquad_struct * B )
{	double resultL;
	int test;
	
	do
	{   //  biquad filter operation :
	resultL = B->A0* inputL + B->A1*B->rLx_1 + B->A2*B->rLx_2 - B->B1*B->rLy_1 - B->B2*B->rLy_2;
	B->rLx_2 = B->rLx_1;
	B->rLx_1 = inputL;
	B->rLy_2 = B->rLy_1;
	B->rLy_1 = inputL = resultL;	// setup inputL for next stage
	
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return resultL;				// last cascaded output to caller
	
}	// Mono_IIR_Perform()


double SquaredStereo_IIR_Perform ( double inputL, double inputR, Rbiquad_struct * B ) // outputs squared stereo filter amplitude
{	double resultL,resultR;
	int test;
	
	do
	{   //  biquad filter operation :
	resultL = B->A0* inputL + B->A1*B->rLx_1 + B->A2*B->rLx_2 - B->B1*B->rLy_1 - B->B2*B->rLy_2;
	B->rLx_2 = B->rLx_1;
	B->rLx_1 = inputL;
	B->rLy_2 = B->rLy_1;
	B->rLy_1 = inputL = resultL;	// setup inputL for next stage
	
    // right channel:
	resultR = B->A0* inputR + B->A1*B->rRx_1 + B->A2*B->rRx_2 - B->B1*B->rRy_1 - B->B2*B->rRy_2;
	B->rRx_2 = B->rRx_1;
	B->rRx_1 = inputR;
	B->rRy_2 = B->rRy_1;
	B->rRy_1 = inputR = resultR;	// setup inputR for next stage
		
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return (resultL*resultL + resultR*resultR);	// squared amplitude output to caller
	
}	// SquaredStereo_IIR_Perform()


#if 0
double Real_IIR_Perform ( double input, int iir_select )    // not used
{	double result;
    Rbiquad_struct * B = & Station_IIR_Filter[iir_select] [0];
	int test;
	
	do
	{   //  biquad filter operation :
	result = B->A0* input + B->A1*B->rLx_1 + B->A2*B->rLx_2 - B->B1*B->rLy_1 - B->B2*B->rLy_2;
	B->rLx_2 = B->rLx_1;
	B->rLx_1 = input;
	B->rLy_2 = B->rLy_1;
	B->rLy_1 = input = result;	// setup input for next stage
	
	if ((test=B->section) > 0) B++;
	
	} while (test > 0);

	return result;				// last cascaded output to caller
	
}	// Real_IIR_Perform()
#endif

//---------------------------------------------------



#endif // #ifndef	_iir_real_filtercode_h_
