SAQrx v0.98 written by Johan Bodin, SM6LKM, enhanced by SWL Roland Fröhlich
First version by Johan (v0.6) were a 44 kHz version only. 
Roland created two additional software versions with 96 kHz and 192 kHz sampling rate.

Starting with SAQrx

1) Choose one of the SAQrx applications

  Choose the one suited to your sound card's sampling rate (44k, 96k or 192k)

2) Set the sampling rate of the sound card used

  Windows: 
  - open the control panel or the settings app
  - move to hardware-related themes and choose sound settings
  - choose the sound card you want to use with SAQrx and go to the **record** section where "microphone" and "line" are located
  - choose 96000 Hz sampling rate for instance

  Ubuntu:
  Ubuntu is running audio services based on the pulseaudio server.
  For setting the sampling rate, you must modify one of the pulseaudio configuration files, for instance daemon.conf:

  /etc/pulse/daemon.conf

  default-sample-rate = 96000
  alternate-sample-rate = 48000

3) Running SAQrx

  Be sure to have the file "frequencies.txt" in the working directory of SAQrx (the place you start it from). 

  - Windows: double-click it
  - Ubuntu: start SAQrx with wine: "wine SAQrxV098_96k.exe"

  If the sound is crackling or SAQrx came to a halt, you should think of not using the oldest PC you own. :-)

4) Transmitter information

  If you move the mouse cursor over the spectrum part of the window, information from frequencies.txt will be displayed.
  Click on the screen to move the passband to the transmitter identified.


5) Recording

  Always use *** Record RF ***!!
  "Record RF" saves the whole bandwidth into a .wav file without any compression.
  So you are able to analyze the record using other tools later without loss in quality with the original sampling rate preserved.

  You are able to replay the RF file with SAQrx, too. Just click "Play File" to open a file dialog.
