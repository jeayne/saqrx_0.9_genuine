Remarks on SAQrx V0.9 
=====================

1) Compiler is Dev-C++5.2.0.1 (old has missing operators)

2) Compile procedure:
   1) Edit saq_config.h for samplerate and kHz ticks
   2) Compiler/Project/Project Options/ Build Options : build name for *.exe file
   3) Compiler/Execute : Rebuild All
   
------------------------------------

Used tricks:

The data for FIR filtering are allocated in a modulo 2^n buffer, 
no compare and conditional jumps are needed, result is faster execution.

In init of portaudio a second try is made if first failed. Possible samplerate conflicts resolved.
But in rare cases the samplerate is still wrong, seeing in truncated RF spectrum.
Terminate then and try again.
   
------------------------------------

ToDo:

Better documentation, I added only a few graphics of new filters, not complete.
Only interesting for students (or co-authors).

Make FIR filter stuff with FFT convolution instead of direct convolution for lesser CPU load.
Crucial work, not in the first place for a hobby project.

LSB/USB selection. Is easy, only boundary checks are to implement at various places.

More interesting for me:
Morsedecoder - I will never learn it!
But we need a new text window and controls (sliders?)
And a good algorithm, cwget is not good enough for decoding.

-------------------------------------

05.07.2012 Roland.Froehlich@t-online.de



