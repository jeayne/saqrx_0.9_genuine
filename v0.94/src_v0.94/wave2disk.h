/*	wave2disk.h				15.05.2012 06:08

	R.Froehlich

	Caution: In an ASIO driver environment the blocksize of audio buffer can be unequal 2^n !
	
	per file: MAX_WAV_SHORT_ANZ for 60 min MONO at 44.1kHz, ca. 316 MB
	auto write next file
*/

#ifndef	_wave2disk_h	
#define	_wave2disk_h

#include	"saq_config.h"


#if	MORSE_SEND
#define	RF_PREFIX	".\\SIM_RF"	// all wave files written to disk are prefixed with this string, then date and time.
#define	AF_PREFIX	".\\SIM_AF"	// all wave files written to disk are prefixed with this string, then date and time.
#else
#define	RF_PREFIX	".\\RF"	// all wave files written to disk are prefixed with this string, then date and time.
#define	AF_PREFIX	".\\AF"	// all wave files written to disk are prefixed with this string, then date and time.

#endif


// count shorts, for a buffered write operation, made in GUI periodically (Timer-Event) :
#define	WAVEOUT_BUFMAX	(1024*8)	

// maximal 2 GB for filelength in *.wav files allowed, ca. 3h in Stereo, 6h MONO,  and Fs=44.1kHz :
//#define MAX_WAV_SHORT_ANZ ((1<<30)- WAVEOUT_BUFMAX)		

// test the method for maximal length check (it is checked as OK) : 
//#define MAX_WAV_SHORT_ANZ ((1<<23)- WAVEOUT_BUFMAX)	// ca. 1.5 min, ca. 16 MB Stereo

//#define MAX_WAV_SHORT_ANZ ( 60*44100)		// ca.  1 min MONO at 44.1kHz, ca. 5.3 MB
#define MAX_WAV_SHORT_ANZ ( 60*60*44100)	// ca. 60 min MONO at 44.1kHz, ca. 316 MB

// static storage area in the class RECORD_WAVE_class,
// 128 K samples (as short) linear buffer, partioned  in buffers of WAVEOUT_BUFMAX length 
// the parts are adjusted later (in the first access) for easy modulo block access in the routine Set_Frames() :
#define	LINWAVBUF_SIZE	(128*1024)	


#ifdef __cplusplus
extern "C" {
#endif

//	Interface functions for main:
//	=============================

void Toggle_Record_Switch_RF (int audiochannels);	// in GUI called, 1 for MONO, 2 for STEREO
void Toggle_Record_Switch_AF (int audiochannels);	// in GUI called

int	Record_Status_RF(void);
int	Record_Status_AF(void);

void Record_WaveStereo_RF ( float* src, int framelength);	// fast process, in Audio Callback
void Record_WaveStereo_AF( float* src, int framelength);	// fast process, in Audio Callback

void Poll_Recorder_toDisk  (void);	// slow process, periodically in GUI called

char* Get_Record_TapeTime_RF(void);	// return: hhh:mm:ss  as 9 byte textstring
char* Get_Record_TapeTime_AF(void);

int get_UTC( char * time_txt);		// returned char counter
int get_UTC_date( char * time_txt);	// returned char counter
int get_datetimeZ( char * time_txt, int zulu0);	// returned char counter, zulu0=0 for UTC, else local time

#ifdef __cplusplus
}
#endif

#endif	// _wave2disk_h
