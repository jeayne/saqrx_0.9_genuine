/*	saq_config.h		05.06.2013 08:28
	/RFR Roland Froehlich
*/

#ifndef	_saq_config_h	
#define	_saq_config_h

//============  CONFIGURATION OF SAMPLERATE ,hardwired  =======>

#define	FS_BASE		44100		// only 44100 or 48000 allowed !
#define	MMSTEPS		22			// 22 (for 44100 etc.) or 24 (for 48000 etc.), major marker steps in kHz until Nyquist
#define	FS_SCALE	1			// 1 or 2 or 4.

//=============================================================<


#define	AF_SCALE	1			// AF Baseband : 1 (or 2 or 4), function of decimation factor DO NOT CHANGE !!!

#define	FS	(FS_SCALE * FS_BASE)    // soundcard sample rate

#define AF_FS   ((double) (AF_SCALE*(FS_BASE/4)))   // Audio band samplerate: 11025 or 12000 Hz

//#define FPB		(256*FS_SCALE)	// requested FPB frames per buffer, let PA determine numBuffers
#define FPB			(512)			// requested FPB frames per buffer, let PA determine numBuffers
//--------------------------------------------------------------------------------------------

#define _DEBUG	0

#define MORSE_SEND		0		// if 1: override input samples and generate a morse code for 17200 Hz(=USB) and LSB.
#define NOISE_ADD		1		// only valid with MORSE_SEND!= 0, make a realistic signal with noise


#define	ASIO_ENVIRONMENT	0	// we are not in an ASIO Host Environment here (this code was ported from an ASIO-Application)		


#ifdef _DEBUG
#define	FTRACE	DebugFileTrace
void DebugFileTrace(char * fmt, ...);
#else
#define	FTRACE
#endif


#define FFT_SIZE_IN   1024    		// time domain input points for the FFT, DO NOT CHANGE !!!


#endif	// _saq_config_h
