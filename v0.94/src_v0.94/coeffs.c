/*		coeffs.c			15.07.2012 08:04
		R.Froehlich
		
	K12_CW1000_B.c	neu TW=100 Hz L=420
	K11_CW50_B.c	neu TW= 25 Hz L=1168 -3dB/-90dB    02.07.2012
	K12_CW50_B.c	neu TW= 25 Hz L=1200 -3dB/-82dB    02.07.2012
	dw100.c			15.07.2012 for CW50 minphase decoding
*/

#include <math.h> 
#include "saq_config.h"
#include "coeffs.h"

Filter_typ CW_varShift_I;	// variable shift for CW filters
Filter_typ CW_varShift_Q;	// variable shift for CW filters

// located in main -->

extern int mph_flag;		// minimum-phase flag

//extern float Hz_VFO;
// Filter characteristics :
extern float Hz_SH;		// offset passband middle BW/2 to VFO : SH = f1 + BW/2 
extern float Hz_BW;		// width of passband BW
extern float Hz_f1;		// lower corner passband after shift
extern float Hz_f2;		// upper corner passband after shift, f1 < f2= f1+BW, for upper sideband.
						// used for USB Mode only:
extern float Hz_VFO_min;	// = 0.0
extern float Hz_VFO_max;	// =(0.5*FS - Hz_f2) // Hz_VFO + f2 <= NYQ

extern float shift_hz_requested;
extern float Hz_SH_min;
extern float Hz_SH_max;
extern float Hz_TW;		// transition bandwidth of CW filters
extern float Hz_DecBW;	// Decimation filter bandwidth

extern Ptr_Filter_typ	pDEC_FILTER;	// the IP_Factor  must be the same for both filters !!!
extern Ptr_Filter_typ	pINT_FILTER;	// but the tap count can be different

extern Ptr_Filter_typ	pUSR_FILTER_I;	// Quadrature Filter for selectivity in Fs/IP, I-phase
extern Ptr_Filter_typ	pUSR_FILTER_Q;	// Quadrature Filter for selectivity in Fs/IP, Q-phase

extern int afpix1;
extern int afpix2;

// forward reference: these are here located (in this file):
// -------------------------------------------------------->

extern Filter_typ  IPxx_AM;		   
extern Filter_typ  IPxx_SSB;       
extern Filter_typ  IPxx_CW; 

extern Filter_typ  CW1000_protoLP; 	// prototyp lowpass, for realtime shifting used           
extern Filter_typ  CW300_protoLP;       
extern Filter_typ  CW100_protoLP;             
extern Filter_typ  CW50_protoLP; 
extern Filter_typ  SSB_protoLP;
extern Filter_typ  AM_protoLP; 

Filter_typ  Alterable_I;	// here is the result of shifted complex filter stored, for use in audio callback   
Filter_typ  Alterable_Q;         
//------------------------------------------
Filter_typ * FiltAdr( int filt_enum);
void Make_Shift_IQ ( Ptr_Filter_typ proto_lowpass, Ptr_Filter_typ hI, Ptr_Filter_typ hQ, double Fshift ,double Fs);


#define AM_Hz_DecBW   4500.0	// Decimation filter bandwidth  , maximum of all decimation filters


void FilterIQ_Set (int casus)		// called also if cw pitch changed
{	float diff_sh;					// used for pitch tuning
	static int oldmode = 0;			// for trigger of changes of vfo
	
	bool nochange_vfo = ((oldmode >= 4)&&( casus <= 3));	// ssb/am --> cw: vfo is not altered, only checked.

	switch (casus)
	{
	case 0:	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_CW); Hz_DecBW  = 1000.0;	// at low pitch select CW decimator	

			Hz_BW	  =   50;			// width of passband BW
			Hz_TW     =   25; // _B version! 50;			// transition bandwidth of CW filters
			
			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = AM_Hz_DecBW -0.5*Hz_BW;	// allow the highest pitch possible
//			Hz_SH_min = 600; Hz_SH_max = 4000;	// fixed: for all the same value!
			
			
			if ( shift_hz_requested < Hz_SH_min) shift_hz_requested = Hz_SH_min;	// map into correct range
			if ( shift_hz_requested > Hz_SH_max) shift_hz_requested = Hz_SH_max;			
			
			if ( shift_hz_requested > 2200 )
			{	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_AM); Hz_DecBW  = 4500.0;	} 	// at highest pitch select AM decimator
			else
			if ( shift_hz_requested >  850 )
			{	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_SSB); Hz_DecBW  = 2700.0;	} 	// at middle pitch select SSB decimator	
			
//			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = Hz_DecBW -0.5*Hz_BW;	// update possible changes
						
			diff_sh = Hz_SH - shift_hz_requested;	// remember for QSY			
			Hz_SH = shift_hz_requested;				// set the new pitch and construct IQ filters:				
			Make_Shift_IQ ( FiltAdr(filt_CW50), &Alterable_I, &Alterable_Q, Hz_SH , AF_FS);			
			pUSR_FILTER_I =	&Alterable_I; pUSR_FILTER_Q = &Alterable_Q;
						
//			pUSR_FILTER_I = & K12_CW50_A_I; 	
//			pUSR_FILTER_Q = & K12_CW50_A_Q; 				
//			Hz_SH=  700;				// offset passband middle BW/2 to VFO : SH = f1 + BW/2 

			Hz_f1=  Hz_SH-0.5*Hz_BW;	// lower corner passband after shift
			Hz_f2=  Hz_f1+Hz_BW;		// upper corner passband after shift, f1 < f2= f1+BW, for upper sideband.								
			Hz_VFO_min= 0.0;			// used for USB Mode only:
			Hz_VFO_max=(0.5*FS - Hz_f2);// Hz_VFO + f2 <= NYQ
			
// adjust VFO : vfo1 + sh1 = carrier = vfo2 + sh2 ==> vfo2 -vfo1 = sh1 - sh2, carrier must be hold in all CW modes
			if  (nochange_vfo) diff_sh = 0.0;  
			vQSY ( diff_sh );			// in main: Change LO frequency by Offset and check range in [f1,f2] !
			
			break;

	case 1:	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_CW); Hz_DecBW  = 1000.0;	// Decimation filter bandwidth

			Hz_BW	  =   100;			// width of passband BW
			Hz_TW     =   100;			// transition bandwidth of CW filters
			
			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = AM_Hz_DecBW -0.5*Hz_BW;
//			Hz_SH_min = 600; Hz_SH_max = 4000;	// fixed: for all the same value!
						
			if ( shift_hz_requested < Hz_SH_min) shift_hz_requested = Hz_SH_min;	// map into correct range
			if ( shift_hz_requested > Hz_SH_max) shift_hz_requested = Hz_SH_max;			
			
			if ( shift_hz_requested > 2200 )
			{	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_AM); Hz_DecBW  = 4500.0;	} 	// Decimation filter bandwidth
			else
			if ( shift_hz_requested >  850 )
			{	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_SSB); Hz_DecBW  = 2700.0;	} 	// Decimation filter bandwidth	
			
//			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = Hz_DecBW -0.5*Hz_BW;	// update possible changes
						
			diff_sh = Hz_SH - shift_hz_requested;	// remember for QSY			
			Hz_SH = shift_hz_requested;				
			Make_Shift_IQ ( FiltAdr(filt_CW100), &Alterable_I, &Alterable_Q, Hz_SH , AF_FS);			
			pUSR_FILTER_I =	&Alterable_I; pUSR_FILTER_Q = &Alterable_Q;
										
			Hz_f1=  Hz_SH-0.5*Hz_BW;	// lower corner passband after shift
			Hz_f2=  Hz_f1+Hz_BW;		// upper corner passband after shift, f1 < f2= f1+BW, for upper sideband.								
			Hz_VFO_min= 0.0;			// used for USB Mode only:
			Hz_VFO_max=(0.5*FS - Hz_f2);// Hz_VFO + f2 <= NYQ
			
// adjust VFO : vfo1 + sh1 = carrier = vfo2 + sh2 ==> vfo2 -vfo1 = sh1 - sh2, carrier must be hold
			if  (nochange_vfo) diff_sh = 0.0;  
			vQSY ( diff_sh );			// in main: Change LO frequency by Offset and check range !
			
			break;


	case 2:	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_CW); Hz_DecBW  = 1000.0;	// Decimation filter bandwidth

			Hz_BW	  =   300;			// width of passband BW
			Hz_TW     =   100;			// transition bandwidth of CW filters
			
			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = AM_Hz_DecBW -0.5*Hz_BW;
//			Hz_SH_min = 600; Hz_SH_max = 4000;	// fixed: for all the same value!
						
			if ( shift_hz_requested < Hz_SH_min) shift_hz_requested = Hz_SH_min;	// map into correct range
			if ( shift_hz_requested > Hz_SH_max) shift_hz_requested = Hz_SH_max;			
			
			if ( shift_hz_requested > 2200 )
			{	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_AM); Hz_DecBW  = 4500.0;	} 	// Decimation filter bandwidth
			else
			if ( shift_hz_requested >  850 )
			{	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_SSB); Hz_DecBW  = 2700.0;	} 	// Decimation filter bandwidth	
			
//			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = Hz_DecBW -0.5*Hz_BW;	// update possible changes
						
			diff_sh = Hz_SH - shift_hz_requested;	// remember for QSY			
			Hz_SH = shift_hz_requested;				
			Make_Shift_IQ ( FiltAdr(filt_CW300), &Alterable_I, &Alterable_Q, Hz_SH , AF_FS);			
			pUSR_FILTER_I =	&Alterable_I; pUSR_FILTER_Q = &Alterable_Q;
										
			Hz_f1=  Hz_SH-0.5*Hz_BW;	// lower corner passband after shift
			Hz_f2=  Hz_f1+Hz_BW;		// upper corner passband after shift, f1 < f2= f1+BW, for upper sideband.								
			Hz_VFO_min= 0.0;			// used for USB Mode only:
			Hz_VFO_max=(0.5*FS - Hz_f2);// Hz_VFO + f2 <= NYQ
			
// adjust VFO : vfo1 + sh1 = carrier = vfo2 + sh2 ==> vfo2 -vfo1 = sh1 - sh2, carrier must be hold
			if  (nochange_vfo) diff_sh = 0.0;  
			vQSY ( diff_sh );			// in main: Change LO frequency by Offset and check range !
			
			break;


	case 3:	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_SSB); Hz_DecBW  = 2700.0;	// Decimation filter bandwidth  

			Hz_BW	  =   1000;			// width of passband BW
			Hz_TW     =   100;			// transition bandwidth of CW filters
			
			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = AM_Hz_DecBW -0.5*Hz_BW;
//			Hz_SH_min = 600; Hz_SH_max = 4000;	// fixed: for all the same value!
						
			if ( shift_hz_requested < Hz_SH_min) shift_hz_requested = Hz_SH_min;	// map into correct range
			if ( shift_hz_requested > Hz_SH_max) shift_hz_requested = Hz_SH_max;			
			
			if ( shift_hz_requested > 2200 )
			{	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_AM); Hz_DecBW  = 4500.0;	} 	// Decimation filter bandwidth
//			else
//			if ( shift_hz_requested >  850 )
//			{	pDEC_FILTER = pINT_FILTER = & IPxx_SSB; Hz_DecBW  = 2700.0;	} 	// Decimation filter bandwidth	
			
//			Hz_SH_min = Hz_TW    +0.5*Hz_BW; Hz_SH_max = Hz_DecBW -0.5*Hz_BW;	// update possible changes
						
			diff_sh = Hz_SH - shift_hz_requested;	// remember for QSY			
			Hz_SH = shift_hz_requested;				
			Make_Shift_IQ ( FiltAdr(filt_CW1000), &Alterable_I, &Alterable_Q, Hz_SH , AF_FS);			
			pUSR_FILTER_I =	&Alterable_I; pUSR_FILTER_Q = &Alterable_Q;
										
			Hz_f1=  Hz_SH-0.5*Hz_BW;	// lower corner passband after shift
			Hz_f2=  Hz_f1+Hz_BW;		// upper corner passband after shift, f1 < f2= f1+BW, for upper sideband.								
			Hz_VFO_min= 0.0;			// used for USB Mode only:
			Hz_VFO_max=(0.5*FS - Hz_f2);// Hz_VFO + f2 <= NYQ
			
// adjust VFO : vfo1 + sh1 = carrier = vfo2 + sh2 ==> vfo2 -vfo1 = sh1 - sh2, carrier must be hold
			if  (nochange_vfo) diff_sh = 0.0;  
			vQSY ( diff_sh );			// in main: Change LO frequency by Offset and check range !
			
			break;

			
	case 4:	pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_SSB); Hz_DecBW  = 2700.0;	// Decimation filter bandwidth  

			Hz_SH= 1500;		// offset passband middle BW/2 to VFO : SH = f1 + BW/2 

			Make_Shift_IQ ( FiltAdr(filt_SSB), &Alterable_I, &Alterable_Q, Hz_SH , AF_FS);			
			pUSR_FILTER_I =	&Alterable_I; pUSR_FILTER_Q = &Alterable_Q;
							
//			pUSR_FILTER_I = & Kyy_SSB_A_I;	// SSB case	
//			pUSR_FILTER_Q = & Kyy_SSB_A_Q;				

			Hz_TW=  300;		// transition bandwidth , not used
			Hz_BW= 2400;		// width of passband BW
			Hz_f1=  300;		// lower corner passband after shift
			Hz_f2= 2700;		// upper corner passband after shift, f1 < f2= f1+BW, for upper sideband.								
			Hz_VFO_min= 0.0;	// used for USB Mode only:
			Hz_VFO_max=(0.5*FS - Hz_f2); // Hz_VFO + f2 <= NYQ

			break;
	
	case 5:		
	default: pDEC_FILTER = pINT_FILTER = FiltAdr(filt_IPxx_AM); Hz_DecBW  = 4500.0;	// Decimation filter bandwidth 

			Hz_SH= 2250;		// offset passband middle BW/2 to VFO : SH = f1 + BW/2 

			Make_Shift_IQ ( FiltAdr(filt_AM), &Alterable_I, &Alterable_Q, Hz_SH , AF_FS);			
			pUSR_FILTER_I =	&Alterable_I; pUSR_FILTER_Q = &Alterable_Q;
							
//			pUSR_FILTER_I = & Kyy_AM_A_I; // AM case
//			pUSR_FILTER_Q = & Kyy_AM_A_Q; 

			Hz_TW=  100;		// transition bandwidth , not used			
			Hz_BW= 4300;		// width of passband BW
			Hz_f1=  100;		// lower corner passband after shift
			Hz_f2= 4400;		// upper corner passband after shift, f1 < f2= f1+BW, for upper sideband.								
			Hz_VFO_min= 0.0;	// used for USB Mode only:
			Hz_VFO_max=(0.5*FS - Hz_f2); // Hz_VFO + f2 <= NYQ
						
			break;								
	}

	oldmode = casus;	// store current mode

    afpix1 = (int)  ceil  ((Hz_f1 * FFT_SIZE_IN)/AF_FS );	
    afpix2 = (int)  floor ((Hz_f2 * FFT_SIZE_IN)/AF_FS );
            		
}	// FilterIQ_Set()


#ifndef  C_PI
 #define C_PI  (3.1415926535897932384626)
#endif

void Make_Shift_IQ ( Ptr_Filter_typ Hproto, Ptr_Filter_typ hI, Ptr_Filter_typ hQ, double Fshift ,double Fs)
{	int i;
	int filtergrad = Hproto->All_Taps - 1 ;
	double w, beta;
	beta = 	C_PI * Fshift / Fs ;	

	
	for (i=0; i <= filtergrad; i++)
	{	
		w = ( 2*i - filtergrad ) * beta;
	
		hI->FilterCoeffs[i] = Hproto->FilterCoeffs[i]* cos(w);	// Make Real Part (Inphase) Filter:
		hQ->FilterCoeffs[i] = Hproto->FilterCoeffs[i]* sin(w);	// Make Imag Part (Quadrature) Filter:
	}

	hI->All_Taps  = Hproto->All_Taps;
	hQ->All_Taps  = Hproto->All_Taps;

	hI->IP_Factor = Hproto->IP_Factor;
	hQ->IP_Factor = Hproto->IP_Factor;
	
}	// Make_Shift_IQ()
	

void Filters_Init (void)
{
	pDEC_FILTER = & IPxx_SSB;  
	pINT_FILTER = & IPxx_SSB; 
	shift_hz_requested = 750.0;	// only for CW modes applicable 
	FilterIQ_Set (3);
}


// insert the correct samplerate versions here -->
// all versions completed 22.06.2012 07:21
#if (FS_BASE == 48000)
#if (FS_SCALE==1)			// 1 or 2 or 4.
Filter_typ  IPxx_AM	= { 
#include "./filter/IP4_48k_AM_A.c"	                                
};		                                   
Filter_typ  IPxx_SSB   = {                                 
#include "./filter/IP4_48k_SSB_A.c"                                  
};                                     
Filter_typ  IPxx_CW    = {                                 
#include "./filter/IP4_48k_CW_A.c"                                   
}; 

#elif (FS_SCALE==2)
Filter_typ  IPxx_AM	= { 
#include "./filter/IP8_96k_AM_A.c"	                                
};		                                   
Filter_typ  IPxx_SSB   = {                                 
#include "./filter/IP8_96k_SSB_A.c"                                  
};                                     
Filter_typ  IPxx_CW    = {                                 
#include "./filter/IP8_96k_CW_A.c"                                   
}; 
#elif (FS_SCALE==4)
Filter_typ  IPxx_AM	= { 
#include "./filter/IP16_192k_AM_A.c"	                                
};		                                   
Filter_typ  IPxx_SSB   = {                                 
#include "./filter/IP16_192k_SSB_A.c"                                  
};                                     
Filter_typ  IPxx_CW    = {                                 
#include "./filter/IP16_192k_CW_A.c"                                   
}; 
#endif
#endif

#if (FS_BASE == 44100)
// 44.1k version:

#if (FS_SCALE==1)			// 1 or 2 or 4.
Filter_typ  IPxx_AM	= { 
#include "./filter/IP4_44k_AM_A.c"	                                
};		                                   
Filter_typ  IPxx_SSB   = {                                 
#include "./filter/IP4_44k_SSB_A.c"                                  
};                                     
Filter_typ  IPxx_CW    = {                                 
#include "./filter/IP4_44k_CW_A.c"                                   
}; 

#elif (FS_SCALE==2)
Filter_typ  IPxx_AM	= { 
#include "./filter/IP8_88k_AM_A.c"	                                
};		                                   
Filter_typ  IPxx_SSB   = {                                 
#include "./filter/IP8_88k_SSB_A.c"                                  
};                                     
Filter_typ  IPxx_CW    = {                                 
#include "./filter/IP8_88k_CW_A.c"                                   
}; 
#elif (FS_SCALE==4)
Filter_typ  IPxx_AM	= { 
#include "./filter/IP16_176k_AM_A.c"	                                
};		                                   
Filter_typ  IPxx_SSB   = {                                 
#include "./filter/IP16_176k_SSB_A.c"                                  
};                                     
Filter_typ  IPxx_CW    = {                                 
#include "./filter/IP16_176k_CW_A.c"                                   
}; 
#endif


#endif


#if (FS_BASE == 48000)
                                    
Filter_typ  AM_protoLP      = {                                 
#include "./filter/K12_AM_A.c"                                     
};                                                                        
Filter_typ  SSB_protoLP     = {                                 
#include "./filter/K12_SSB_A.c"                                    
};   

Filter_typ  CW1000_protoLP  = {                                 
//#include "./filter/K12_CW1000_A.c"  	// transition width = 200Hz     L=212   
#include "./filter/K12_CW1000_B.c"   	// transition width = 100Hz 	L=420                              
};                                     
                                                                      
Filter_typ  CW300_protoLP   = {                                 
#include "./filter/K12_CW300_A.c"                                  
};                                     
Filter_typ  CW100_protoLP   = {                                 
#include "./filter/K12_CW100_A.c"                                  
};                                     
                                    
Filter_typ  CW50_protoLP    = {                                 
#include "./filter/K12_CW50_B.c"                                   
};                                     

#else
// 44.1k version:                                    
Filter_typ  AM_protoLP      = {                                 
#include "./filter/K11_AM_A.c"                                     
};                                                                         
Filter_typ  SSB_protoLP     = {                                 
#include "./filter/K11_SSB_A.c"                                    
};                                     
Filter_typ  CW1000_protoLP  = {                                  
#include "./filter/K11_CW1000_B.c"   	// transition width TW= 100Hz 	L=386                              
};                                                                                                         
Filter_typ  CW300_protoLP   = {                                 
#include "./filter/K11_CW300_A.c"                                  
};                                     
Filter_typ  CW100_protoLP   = {                                 
#include "./filter/K11_CW100_A.c"                                  
};                                                                       
Filter_typ  CW50_protoLP    = {                                 
#include "./filter/K11_CW50_B.c"                                   
};                                     

#endif


// all minimum_phase filters =================>>>>
// insert the correct samplerate versions here -->
// all versions completed 24.06.2012 17:01
#if (FS_BASE == 48000)
#if (FS_SCALE==1)			// 1 or 2 or 4.
Filter_typ  mIPxx_AM	= { 
#include "./mfilter/IP4_48k_AM_A.c"	                                
};		                                   
Filter_typ  mIPxx_SSB   = {                                 
#include "./mfilter/IP4_48k_SSB_A.c"                                  
};                                     
Filter_typ  mIPxx_CW    = {                                 
#include "./mfilter/IP4_48k_CW_A.c"                                   
}; 

#elif (FS_SCALE==2)
Filter_typ  mIPxx_AM	= { 
#include "./mfilter/IP8_96k_AM_A.c"	                                
};		                                   
Filter_typ  mIPxx_SSB   = {                                 
#include "./mfilter/IP8_96k_SSB_A.c"                                  
};                                     
Filter_typ  mIPxx_CW    = {                                 
#include "./mfilter/IP8_96k_CW_A.c"                                   
}; 
#elif (FS_SCALE==4)
Filter_typ  mIPxx_AM	= { 
#include "./mfilter/IP16_192k_AM_A.c"	                                
};		                                   
Filter_typ  mIPxx_SSB   = {                                 
#include "./mfilter/IP16_192k_SSB_A.c"                                  
};                                     
Filter_typ  mIPxx_CW    = {                                 
#include "./mfilter/IP16_192k_CW_A.c"                                   
}; 
#endif
#endif

#if (FS_BASE == 44100)
// 44.1k version:

#if (FS_SCALE==1)			// 1 or 2 or 4.
Filter_typ  mIPxx_AM	= { 
#include "./mfilter/IP4_44k_AM_A.c"	                                
};		                                   
Filter_typ  mIPxx_SSB   = {                                 
#include "./mfilter/IP4_44k_SSB_A.c"                                  
};                                     
Filter_typ  mIPxx_CW    = {                                 
#include "./mfilter/IP4_44k_CW_A.c"                                   
}; 

#elif (FS_SCALE==2)
Filter_typ  mIPxx_AM	= { 
#include "./mfilter/IP8_88k_AM_A.c"	                                
};		                                   
Filter_typ  mIPxx_SSB   = {                                 
#include "./mfilter/IP8_88k_SSB_A.c"                                  
};                                     
Filter_typ  mIPxx_CW    = {                                 
#include "./mfilter/IP8_88k_CW_A.c"                                   
}; 
#elif (FS_SCALE==4)
Filter_typ  mIPxx_AM	= { 
#include "./mfilter/IP16_176k_AM_A.c"	                                
};		                                   
Filter_typ  mIPxx_SSB   = {                                 
#include "./mfilter/IP16_176k_SSB_A.c"                                  
};                                     
Filter_typ  mIPxx_CW    = {                                 
#include "./mfilter/IP16_176k_CW_A.c"                                   
}; 
#endif


#endif


#if (FS_BASE == 48000)
                                    
Filter_typ  mAM_protoLP      = {                                 
#include "./mfilter/K12_AM_A.c"                                     
};                                                                        
Filter_typ  mSSB_protoLP     = {                                 
#include "./mfilter/K12_SSB_A.c"                                    
};   

Filter_typ  mCW1000_protoLP  = {                                 
//#include "./mfilter/K12_CW1000_A.c"  	// transition width = 200Hz     L=212   
#include "./mfilter/K12_CW1000_B.c"   	// transition width = 100Hz 	L=420                              
};                                     
                                                                      
Filter_typ  mCW300_protoLP   = {                                 
#include "./mfilter/K12_CW300_A.c"                                  
};                                     
Filter_typ  mCW100_protoLP   = {                                 
#include "./mfilter/K12_CW100_A.c"                                  
};                                     
                                    
Filter_typ  mCW50_protoLP    = {                                 
//#include "./mfilter/K12_CW50_B.c"   // 15.07.2012 
#include "./filter/dw100.c"                                  
};                                     

#else
// 44.1k version:                                    
Filter_typ  mAM_protoLP      = {                                 
#include "./mfilter/K11_AM_A.c"                                     
};                                                                         
Filter_typ  mSSB_protoLP     = {                                 
#include "./mfilter/K11_SSB_A.c"                                    
};                                     
Filter_typ  mCW1000_protoLP  = {                                  
#include "./mfilter/K11_CW1000_B.c"   	// transition width TW= 100Hz 	L=386                              
};                                                                                                         
Filter_typ  mCW300_protoLP   = {                                 
#include "./mfilter/K11_CW300_A.c"                                  
};                                     
Filter_typ  mCW100_protoLP   = {                                 
#include "./mfilter/K11_CW100_A.c"                                  
};                                                                       
Filter_typ  mCW50_protoLP    = {                                 
//#include "./mfilter/K11_CW50_B.c"		// 15.07.2012  	  
#include "./filter/dw100.c"                                   
};                                     

#endif

// all minimum_phase filters =================<<<<<




Filter_typ * FiltAdr( int filt_enum)
{

	switch (filt_enum)
	{
	case filt_CW50:		if (mph_flag) return &mCW50_protoLP  	; else return &CW50_protoLP 	; break;
	case filt_CW100:	if (mph_flag) return &mCW100_protoLP  	; else return &CW100_protoLP 	; break;
	case filt_CW300:	if (mph_flag) return &mCW300_protoLP  	; else return &CW300_protoLP 	; break;
	case filt_CW1000:	if (mph_flag) return &mCW1000_protoLP	; else return &CW1000_protoLP 	; break;
	case filt_SSB:		if (mph_flag) return &mSSB_protoLP  	; else return &SSB_protoLP 		; break;
	case filt_AM:		if (mph_flag) return &mAM_protoLP  		; else return &AM_protoLP 		; break;
	case filt_IPxx_AM:	if (mph_flag) return &mIPxx_AM  		; else return &IPxx_AM 			; break;	   
	case filt_IPxx_SSB: if (mph_flag) return &mIPxx_SSB  		; else return &IPxx_SSB 		; break;      
	case filt_IPxx_CW:	if (mph_flag) return &mIPxx_CW  		; else return &IPxx_CW 			; break; 		
	}
	return 0; // invalid	
}	// FiltAdr()


    
