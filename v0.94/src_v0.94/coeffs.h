//	coeffs.h		24.06.2012 16:45
//	R.Fr�hlich

#ifndef COEFFS_INCLUDED
#define COEFFS_INCLUDED

// IP16_AM has L= 432 ==>
#define DECI_DATALEN	512		// for modulo addressing, must be power of 2 >= maximal TAP number of Decimation Filters
#define INTP_DATALEN 	128		// """, per phase of interpolation, 2^n >= TAPS/IPfactor

typedef struct Filter_typ_tag 
{
	int  	IP_Factor;				// interpolation or decimation factor,typical = 4/8/16, or 1 for IQ-filters (no meaning)			
    int  	All_Taps;				// number of used taps       
    float	FilterCoeffs[1500];		// my maximal taps, coeffs are defined for index = [0] ..  [All_Taps-1]        
} Filter_typ, * Ptr_Filter_typ ;

#define FIR_LARGEST_POW2	2048	// next 2^n of maximal tap count 1500, for modulo addressing

void Filters_Init (void);			// located in coeffs.c
void FilterIQ_Set (int casus);		// located in coeffs.c
void vQSY (float rOffset);			// in main: Change LO frequency by Offset and check range !

enum
{
	filt_CW50,		// = 0
	filt_CW100,
	filt_CW300,
	filt_CW1000, filt_CWmax=filt_CW1000,
	filt_SSB,
	filt_AM,	 filt_MAX=filt_AM,		// until here: range of filter_case

	filt_IPxx_AM,		   
	filt_IPxx_SSB,       
	filt_IPxx_CW, 

};

#endif  // COEFFS_INCLUDED
