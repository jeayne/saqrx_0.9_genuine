
New in V094 (07.01.2014  )
-----------
    File input for 8/16/24 bit play file, Broadcast Wave Format BWF allowed, extra chunks skipped.

    'dB' display range can be shifted by 10dB with 'v'=down or 'V'=up

New in V093 
-----------

    Supported samplerates:

        44.1k/48k/88.2k/96k/176.4k/192k
        The program forces this selection and is happy
        if the soundcard can support this, otherwise you get an error.
    All heterodyne decoding is made in the Upper Side Band (USB).
    
    Filter for CW (BW 1000,300,100,50 Hz), SSB (BW 2400 Hz), AM (BW 4300 Hz)
    Spectrum display (blue) for audio frequency
    File-I/O for *.wav format (16bit)
    Left/Right channel select, and sum or difference L+R,L-R
    RMS bar (red) for audio level, blue bar for inband SNR (Max-Min) after filter 
    Muting (M) key
    Time+Date display, UTC or local time

    Timer function for predefined recording via command line:       
       SAQrxV09_44k.exe [-i] [-o] -txx,yy
       -i : Input  Record (Radio Frequency in FS), or -b for stereo input.
       -o : Output Record (Audio Frequency, 11 or 12kHz)
       xx : Number of minutes to wait for recording after program start
       yy : Number of minutes as duration of recording session                               
    After start, select frequency, filter, level, then wait ...

    For nerds:  Simulation of a SAQ transmission (2007 Christmas message)      
    
    Sources are included for your own studies or development,
    free for noncommercial usage. 
        
    